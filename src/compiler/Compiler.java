package compiler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Compiler {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws EOFexception{
        try {
            Symboltable table = new Symboltable();
            // TODO code application logic here
            File loc = new File("program/if.txt");
//            File loc = new File("parse/gp3.txt");
            
//            File loc = new File("parse/gp1.txt");
//            File loc = new File("parse/gp2.txt");
//            File loc = new File("parse/gp4.txt");
//            File loc = new File("parse/gp5.txt");
//            File loc = new File("parse/gp6.txt");
//            File loc = new File("parse/gp7.txt");
//            File loc = new File("parse/gp8.txt");
//            File loc = new File("parse/gp9.txt");
//            File loc = new File("parse/gp10.txt");
//            File loc = new File("parse/gp11.txt");
//            File loc = new File("parse/gp12.txt");
//            File loc = new File("parse/gp13.txt");
//            File loc = new File("parse/gp14.txt");
//            File loc = new File("parse/gp15.txt");
            
//            File loc = new File("parse/b1.txt");
//            File loc = new File("parse/b2.txt");
//            File loc = new File("parse/b3.txt");
//            File loc = new File("parse/b4.txt");
//            File loc = new File("parse/b5.txt");
//            File loc = new File("parse/b6.txt");
            Parser lalr1 = new Parser(loc, table);
            lalr1.printAllTokens();
            
            System.out.println("\n");
            System.out.println("------------- START OF SYNTAX ANALYZER PHASE -----------------------");
            System.out.println("\n");
            lalr1.createTree();
            Tree parseTree =  lalr1.parseTree.root.Children.get(0);
            System.out.println(parseTree);
            System.out.println("\n");
            System.out.println("------------------------- END OF SYNTAX ANALYZER PHASE -------------------------");
            System.out.println("\n");
            
            System.out.println("------Symbol Table---------");
            System.out.println("\n");
            
            printSymbolTable(lalr1.table);
            
            
            System.out.println("\n");
            System.out.println("----- END OF SYMBOL TABLE -----------");
            System.out.println("\n");
            
            System.out.println("\n");
            System.out.println("----- START OF INTERPRETER PHASE -----------");
            System.out.println("\n");
            
            Interpreter interpreter = new Interpreter(parseTree, lalr1.table);
            interpreter.Interpret();
            
            System.out.println("\n");
            System.out.println("----- END OF INTERPRETER PHASE -----------");
            System.out.println("\n");
            
            
            
        } catch (FileNotFoundException ex) {
            System.out.println("\n\n\n");
            System.out.println("No file found");
        } catch (IOException ex) {
            System.out.println("\n\n\n");
            Logger.getLogger(Compiler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (LEXexception ex) {
            System.out.println("\n\n\n");
            System.out.println("Lexical error. Unexpected Finish");
            System.out.println(ex.error);
        } catch (PARSEexception ex) {
            
            System.out.println("\n\n\n");
            System.out.println("Parse error. Unexpected Finish");
            System.out.println(ex.error);
        } catch (SemanticException ex) {
            System.out.println("\n\n\n");
            System.out.println("Semantic Error. Unexpected Finish");
            System.out.println(ex.error);
            
        }
    }
    
    public static void printSymbolTable(Symboltable table){
        for(String key : table.list.keySet()){
                Identifier entry = table.list.get(key);
                System.out.print("name : " + entry.name + "\t" );
                System.out.print("named constant : " + entry.isFinal + "\t");
                System.out.print("datatype : "  + entry.isDatatype());
                if(entry.list != null){
                    for(String localKey : entry.list.keySet()){
                        System.out.println("");
                        Identifier entryLocal = entry.list.get(localKey);
                        System.out.print("\t"  +"name : " + entryLocal.name + "\t" );
                        System.out.print("named constant : " + entryLocal.isFinal + "\t");
                        System.out.print("datatype : "  + entryLocal.isDatatype() + "\t");
                        if(entryLocal.isFinal == 1){
                            System.out.print("value : " + entryLocal.value);
                        }
                    }
                }
                System.out.println("");
            }
    }
    
}
