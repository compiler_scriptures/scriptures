package compiler;

public class Token {
    public Type  name;
//    public int value; //index number in symbol table. Unsure of this field yet.
    public String lexeme;
    
    Token(Type t, String lex){
        name = t;
        lexeme = lex;
    }
}
