package compiler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LexAnalyzer{
    private BufferedReader read;
    public int line = 1;
    private int charRead = 0;
    private final File input;
    private int lastTokenIndex = 0;
    private Symboltable table;
    private final ReservedWords reservedTable;
    private boolean verbose = true;
    
    LexAnalyzer(File in, Symboltable t, boolean silent) throws FileNotFoundException{
        input = in;
        table = t;
        verbose = !silent;
        read = new BufferedReader(new FileReader(in));
        reservedTable = new ReservedWords();
    }
    
    
    public Token getToken() throws IOException, EOFexception{
        Token passBack;
        DFAvalues temp = new DFAvalues(false, Type.ERR, ""), temp2 = null;
        for(int DFA = 1; DFA <= 24; DFA++){
            switch(DFA){
                    case 1 :
                        temp2 = isWS();
                        break;
                    case 2 :
                        temp2 = isLEFTP();
                        break;
                    case 3 :
                        temp2 = isRIGHTP();
                        break;
                    case 4 : 
                        temp2 = isEos();
                        break;
                    case 5 :
                        temp2 = isLEFTSQ();
                        break;
                    case 6 : 
                        temp2 = isRIGHTSQ();
                        break;
                    case 7 :
                        temp2 = isCOMMA();
                        break;
                    case 8 :
                        temp2 = isRELOPne();
                        break;  
                    case 9 :
                        temp2 = isLEFTCU();
                        break;
                    case 10 :
                        temp2 = isRIGHTCU();
                        break;
                    case 11 :
                       temp2 = isChar();
                       break;
                    case 12:
                        temp2 = isBool();
                        break;
                    case 13 :
                        temp2 = isNum();
                        break;
                    case 14 :
                        temp2 = isLiteral();
                        break;
                    case 15:
                        temp2 = isOPplus();
                        break;
                    case 16:
                        temp2 = isOPminus();
                        break;
                    case 17:
                        temp2 = isOPtimes();
                        break;
                    case 18:
                        temp2 = isOPneg();
                        break;
                    case 19 :
                        temp2 = isRELOPgt();
                        break;
                    case 20 :
                        temp2 = isRELOPlt();
                        break;
                    case 21 :
                        temp2 = isRELOPeq();
                        break;
                    case 22 :
                        temp2 = isRELOPge();
                        break;
                    case 23 :
                        temp2 = isRELOPle();
                        break;    
                    case 24 :
                        temp2 = isID();
                        break;    
            }
//            System.out.println(temp2.tokenType + " " +  temp2.isAccepted + " " + DFA);            
            returnLastTokenIndex();
            temp = (temp.lexeme.length() < temp2.lexeme.length() && temp2.isAccepted) || (temp2.isAccepted && !temp.isAccepted) ? temp2 : temp;
            temp = !temp.isAccepted && !temp2.isAccepted  && temp2.lexeme.length() > temp.lexeme.length()? temp2 : temp; 
        }
        
        if(temp.tokenType.toString().compareTo("ERR") ==0){
            if(temp.lexeme.length() == 0){
                temp.lexeme += readChar();
            }
            return new Token(Type.ERR, temp.lexeme);
        }
        
        
        else if(temp.tokenType.toString().compareTo("Id") == 0 && reservedTable.containsKey(temp.lexeme)){
                Type t = reservedTable.get(temp.lexeme);
                passBack = new Token(t, temp.lexeme);
        }
        else{
            passBack = new Token(temp.tokenType, temp.lexeme);
        }
        //        System.out.println("");
        //        System.out.println(temp.tokenType + " " +  temp.isAccepted + " " + temp.lexeme + " " + temp.lexeme.length());

//        if(passBack.name.toString().compareTo("Id") == 0){
//            table.insert(new Identifier(passBack));
//        }
        
        read.skip(temp.lexeme.length());
        lastTokenIndex += temp.lexeme.length();
        
        return passBack;
    }
    
    public boolean isEOF() throws IOException{
        return peekChar() == (char)(-1);
    }
    
    public char readChar() throws IOException, EOFexception{
        int temp =  read.read();
        char val = (char) temp;
        charRead++;
//        if( temp == -1) throw new EOFexception();
        return val;
    }
    
    public int currentPos(){
        return charRead;
    }
    
    public void unreadChar() throws FileNotFoundException, IOException{
        read.close();
        read = new BufferedReader(new FileReader(input));
        read.skip(--charRead);
    }
    
    public char peekChar() throws IOException{
        read.mark(1);
        char val = (char) read.read();
        read.reset();
        return val;        
    }
    
    public void returnLastTokenIndex() throws IOException{
        read.close();
        read = new BufferedReader(new FileReader(input));
        read.skip(lastTokenIndex);
    }
    
    public DFAvalues isID() throws IOException, EOFexception{
        int start = 0, maybe = 1, isMatch = 2, fail = 3;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0:
                    temp = readChar();
                    if (Character.isLetter(temp)){ 
                        state = maybe;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1:
                    temp = readChar();
                    if (Character.isLetter(temp) || Character.isDigit(temp)){
                        lexeme += temp;
                    }
                    else state = isMatch;
                    break;
                case 2:
                    return new DFAvalues(true, Type.Id, lexeme);
                case 3:
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
       
    public DFAvalues isWS() throws IOException, EOFexception{
        String lexeme = "";
        int start = 0, maybe = 1, isMatch = 2, fail = 3;
        int state = start;
        while(true){
            char temp;
            switch(state){
                case 0:
                    temp = readChar();
                    if (Character.isWhitespace(temp)){
                        if((int)temp == 10){
                            line++;
                            if(verbose)
                                System.out.println("");
                        }
                        state = isMatch;
                        lexeme += temp;
                    }
                    else if(temp == '#'){
                        state = 4;
                        lexeme += temp;
                    }
                    else if(temp == '~'){
                        state = 7;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
            
                case 4 : 
                    temp = readChar();
                    if(temp == '['){
                        state = 5;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 5 :
                    temp = readChar();
                    lexeme += temp;
                    if((int)temp == 10){
                        line++;
                        if(verbose)
                            System.out.println("");
                    }
                    if(temp == ']'){
                        state = 6;
                    }
                    else if((int)temp == 65535){
                        state = fail;
                    }
                    else state = 5;
                    break;
                case 6 :
                    temp = readChar();
                    lexeme += temp;
                    if(temp == '#'){
                        state = isMatch;
                    }
                    else if((int)temp == 65535){
                        state = fail;
                    }
                    else state = 5;
                    break;
                case 7 :
                    temp = readChar();
                    lexeme += temp;
                    if((int)temp == 10){
                        line++;
                        if(verbose)
                            System.out.println("");
                        state = isMatch;
                    }
                    else if((int)temp == 65535){
                        state = fail;
                    }
                    else state = 7;
                    break;
                case 2:
                    return new DFAvalues(true, Type.WHITESPACE, lexeme);
                case 3:
                    return new DFAvalues(false, Type.ERR, lexeme);
                    
                   
            }
        }
    }   
    
    public DFAvalues isLEFTP() throws IOException, EOFexception {
        int start = 0,  isMatch = 1 ,fail = 2;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0: 
                    temp = readChar();
                    if(temp == '('){
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1:
                    return new DFAvalues(true, Type.LEFTP, lexeme);
                case 2:
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
    
    public DFAvalues isRIGHTP() throws IOException, EOFexception {
        int start = 0,  isMatch = 1 ,fail = 2;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0: 
                    temp = readChar();
                    if(temp == ')'){
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1:
                    return new DFAvalues(true, Type.RIGHTP, lexeme);
                case 2:
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
    
    public DFAvalues isLEFTSQ() throws IOException, EOFexception {
        int start = 0,  isMatch = 1 ,fail = 2;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0: 
                    temp = readChar();
                    if(temp == '['){
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1:
                    return new DFAvalues(true, Type.LEFTSQ, lexeme);
                case 2:
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
    
    public DFAvalues isRIGHTSQ() throws IOException, EOFexception {
        int start = 0,  isMatch = 1 ,fail = 2;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0: 
                    temp = readChar();
                    if(temp == ']'){
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1:
                    return new DFAvalues(true, Type.RIGHTSQ, lexeme);
                case 2:
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
    
    public DFAvalues isCOMMA() throws IOException, EOFexception {
        int start = 0,  isMatch = 1 ,fail = 2;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0: 
                    temp = readChar();
                    if(temp == ','){
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1:
                    return new DFAvalues(true, Type.COMMA, lexeme);
                case 2:
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
    
    public DFAvalues isLEFTCU() throws IOException, EOFexception {
        int start = 0,  isMatch = 1 ,fail = 2;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0: 
                    temp = readChar();
                    if(temp == '{'){
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1:
                    return new DFAvalues(true, Type.LEFTCU, lexeme);
                case 2:
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
    
    public DFAvalues isRIGHTCU() throws IOException, EOFexception {
        int start = 0,  isMatch = 1 ,fail = 2;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0: 
                    temp = readChar();
                    if(temp == '}'){
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1:
                    return new DFAvalues(true, Type.RIGHTCU, lexeme);
                case 2:
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
    

    public DFAvalues isChar() throws IOException, EOFexception{
        int start = 0, isMatch = 10, fail = -1;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0:
                    temp = readChar();
                    if (temp=='\''){ 
                        state = 1;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1:
                    temp = readChar();
                    if (Character.isLetter(temp)||Character.isDigit(temp)){ 
                        state = 2;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 2:
                    temp = readChar();
                    if (temp=='\''){ 
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 10:
                    return new DFAvalues(true, Type.Char, lexeme);
                case -1:
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
    
 
    public DFAvalues isNum() throws IOException, EOFexception{
        int start = 0, isMatch = 10, fail = -1;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0:
                    temp = readChar();
                    if (Character.isDigit(temp)){ 
                        state = 1;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1 : 
                    temp = readChar();
                    if(Character.isDigit(temp)){
                        state = 1;
                        lexeme += temp;
                    }
                    else state = isMatch;
                    break;
                case 10:
                    return new DFAvalues(true, Type.Num, lexeme);
                case -1:
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
    
    public DFAvalues isLiteral() throws IOException, EOFexception{
        String symbol = "*!?-_+.,;:<>='()[]{}#&~%$/@^`\\| ";
        int start = 0, isMatch = 10, fail = -1;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0:
                    temp = readChar();
                    if (temp=='"'){ 
                        state = 1;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1:
                    temp = readChar();
                    if (symbol.indexOf(temp)>=0 || Character.isLetter(temp) || Character.isDigit(temp)){ 
                        state = 1;
                        lexeme += temp;
                    }
                    else if (temp=='"'){
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 10:
                    return new DFAvalues(true, Type.Literal, lexeme);
                case -1:
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
    
    public DFAvalues isBool() throws IOException, EOFexception{
        int start = 0, isMatch = 10, fail = -1;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0:
                    temp = readChar();
                    if (temp=='B'){ 
                        state = 1;
                        lexeme += temp;
                    }
                    else if (temp=='G'){ 
                        state = 11;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 11: 
                    temp = readChar();
                    if (temp=='O'){ 
                        state = 12;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 12: 
                    temp = readChar();
                    if (temp=='O'){ 
                        state = 13;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 13: 
                    temp = readChar();
                    if (temp=='D'){ 
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1:
                    temp = readChar();
                    if (temp=='A'){ 
                        state = 2;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 2:
                    temp = readChar();
                    if (temp=='D'){ 
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 10:
                    return new DFAvalues(true, Type.Bool, lexeme);
                case -1:
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
    
    public DFAvalues isEos() throws IOException, EOFexception{
        int start = 0, isMatch = 10, fail = -1;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0:
                    temp = readChar();
                    if (temp==';'){ 
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 10:
                    return new DFAvalues(true, Type.eos, lexeme);
                case -1:
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
    public DFAvalues isOPplus() throws IOException, EOFexception{
        int start = 0,  isMatch = 1 ,fail = 2;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0 : 
                    temp = readChar();
                    if(temp == '+'){ 
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1 :
                    return new DFAvalues(true, Type.OPplus, lexeme);
                case 2 : 
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }public DFAvalues isOPminus() throws IOException, EOFexception{
        int start = 0,  isMatch = 1 ,fail = 2;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0 : 
                    temp = readChar();
                    if(temp == '-'){ 
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1 :
                    return new DFAvalues(true, Type.OPminus, lexeme);
                case 2 : 
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }

    }
    public DFAvalues isOPtimes() throws IOException, EOFexception{
            int start = 0,  isMatch = 1 ,fail = 2;
            int state = start;
            String lexeme = "";
            while(true){
                char temp;
                switch(state){
                    case 0 : 
                        temp = readChar();
                        if(temp == '*'){ 
                            state = isMatch;
                            lexeme += temp;
                        }
                        else state = fail;
                        break;
                    case 1 :
                        return new DFAvalues(true, Type.OPtimes, lexeme);
                    case 2 : 
                        return new DFAvalues(false, Type.ERR, lexeme);
                }
            }
    }
    public DFAvalues isOPneg() throws IOException, EOFexception{
        int start = 0, maybe1 = 1, maybe2 = 2 ,isMatch = 3 ,fail = 4;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0 : 
                    temp = readChar();
                    if(temp == 'N'){ 
                        state = maybe1;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1 :
                    temp = readChar();
                    if(temp == 'E'){
                        state = maybe2;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 2:
                    temp = readChar();
                    if(temp == 'G'){
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 3 :
                    return new DFAvalues(true, Type.OPneg, lexeme);
                case 4 : 
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
    
    public DFAvalues isRELOPgt() throws IOException, EOFexception{
        int start = 0,  isMatch = 1 ,fail = 2;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0 : 
                    temp = readChar();
                    if(temp == '>'){ 
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1 :
                    return new DFAvalues(true, Type.RELOPgt, lexeme);
                case 2 : 
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }

    public DFAvalues isRELOPlt() throws IOException, EOFexception{
        int start = 0,  isMatch = 1 ,fail = 2;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0 : 
                    temp = readChar();
                    if(temp == '<'){ 
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1 :
                    return new DFAvalues(true, Type.RELOPlt, lexeme);
                case 2 : 
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }

    public DFAvalues isRELOPeq() throws IOException, EOFexception{
        int start = 0,  isMatch = 1 ,fail = 2;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0 : 
                    temp = readChar();
                    if(temp == '='){ 
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1 :
                    return new DFAvalues(true, Type.RELOPeq, lexeme);
                case 2 : 
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
    public DFAvalues isRELOPge() throws IOException, EOFexception{
        int start = 0, maybe = 1 , isMatch = 2 ,fail = 3;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0 : 
                    temp = readChar();
                    if(temp == '>'){ 
                        state = maybe;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1 :
                    temp = readChar();
                    if(temp == '='){
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 2 :
                    return new DFAvalues(true, Type.RELOPge, lexeme);
                case 3 : 
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }

    }public DFAvalues isRELOPle() throws IOException, EOFexception{
        int start = 0, maybe = 1 , isMatch = 2 ,fail = 3;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0 : 
                    temp = readChar();
                    if(temp == '<'){ 
                        state = maybe;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1 :
                    temp = readChar();
                    if(temp == '='){
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 2 :
                    return new DFAvalues(true, Type.RELOPle, lexeme);
                case 3 : 
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
public DFAvalues isRELOPne() throws IOException, EOFexception{
        int start = 0, maybe = 1 , isMatch = 2 ,fail = 3;
        int state = start;
        String lexeme = "";
        while(true){
            char temp;
            switch(state){
                case 0 : 
                    temp = readChar();
                    if(temp == '!'){ 
                        state = maybe;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 1 :
                    temp = readChar();
                    if(temp == '='){
                        state = isMatch;
                        lexeme += temp;
                    }
                    else state = fail;
                    break;
                case 2 :
                    return new DFAvalues(true, Type.RELOPne, lexeme);
                case 3 : 
                    return new DFAvalues(false, Type.ERR, lexeme);
            }
        }
    }
}
