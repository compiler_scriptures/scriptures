
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 *
 * @author Josh
 */
public class Tree{
    
   public TreeNode root;
   int height = 0;
   
   Tree(int reductionNum, String data, Stack<Tree> children){ //for reduce
       root = new TreeNode(reductionNum, data, null);
       height = 1;
       int tempHeight = 0;
       while(!children.isEmpty()){
           Tree temp = children.pop();
           tempHeight = Math.max(tempHeight, temp.height);
           insert(temp);
       }
       height += tempHeight;
    }
   
   Tree(String data, String data2){ //for shift
       root = new TreeNode(-1, data, data2);
       height = 1;
   }
   
   private void insert(Tree child){
       child.root.Parent = root;
       root.Children.add(child);
   }
   
   @Override
   public String toString(){ //prefix traversal
       String temp = "[Data : '" + root.SymbolName + "' ";
       for(int i = 0; i < root.Children.size(); i++){
           temp += i+1 + "-child" + root.Children.get(i).toString();
       }
       temp += "]";
       return temp;
   }
   
   public String getTerminal(){
       String temp = "";
       if (root.lexeme!=null){
           temp += root.lexeme + " ";
       }
       for(int i = 0; i < root.Children.size(); i++){
           temp += root.Children.get(i).getTerminal();
       }
       return temp;
   }
   
   public void getTermAndSymb(ArrayList<String[]> symbAndTerm){//[0] = lexee [1] = symbol
       if (root.lexeme!=null){
           symbAndTerm.add( new String[] {root.lexeme, root.SymbolName});
       }
       for(int i = 0; i < root.Children.size(); i++){
           root.Children.get(i).getTermAndSymb(symbAndTerm);
       }
   }
   

  
//   public void reduceConditional(Tree t){
//       //insert a new tree that is operator-precedence-fixed
//   }
   
   
   
//   public String[] postProcess(String[] terminals){
//       ArrayList<String> temp = new ArrayList<>();       
//       for(int i = 0; i < terminals.length; i++){
//           if(terminals[i].contains("\"")){
//               String tempString = terminals[i++];
//               
//               while(i < terminals.length && !terminals[i].contains("\"")){
//                   tempString += " " + terminals[i++];
//               }
//               if (i >= terminals.length) {
//                   i--;
//                   temp.add(tempString.trim());
//               }
//               else{
//                   temp.add((tempString + terminals[i]).trim());
//               }
//           }
//           else{
//               temp.add(terminals[i].trim());
//           }
//       }
//       String[] newTerminals = new String[temp.size()];
//       for(int i = 0; i < temp.size(); i++){
//           newTerminals[i] = temp.get(i);
//       }
//       return newTerminals;
//   }
//   
//   
   
}

