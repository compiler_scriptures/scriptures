package compiler;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;

public class Interpreter{
    public Tree[] parseTrees; //trees per scope. [0] is global scope. Do not alter the trees
    public HashMap<String,Identifier>[] tables; //tables per scope. [0] is global scope. Do not alter the tables except [0].
    public HashMap<String, Identifier> currentTable; // 1 tree : 1 table : 1 scope
    public Stack<Tree> callStack; //this will tell which part of the tree is running
    public Tree currentTree;
    public Stack<HashMap> tableStack;
    public HashMap<String, Integer> mapping; //tells the index based on the scope
    
    public Interpreter(Tree parseTree, Symboltable table) throws SemanticException{
        // do some way to filter data to diff tree parts
        mapping = new HashMap<>();
        tableStack = new Stack();
        callStack = new Stack();
        mapping.put("Global", 0);
        mapping.put("EXODUS", 1);
        int count = 2; //2 since global + main scope
        
        //tagging indeces to scopes
        Tree mList = parseTree.root.Children.get(1).root.Children.get(2);
        if(mList.root.SymbolName.compareTo(Variable.mList.toString()) == 0){
            ArrayList<String[]> symAndTerm = new ArrayList<>();
            mList.getTermAndSymb(symAndTerm);
            for(int i = 0; i < symAndTerm.size(); i++){
                if(symAndTerm.get(i)[1].compareTo(Type.Id.toString()) == 0){
                    mapping.put(symAndTerm.get(i)[0], count++);
                }
            }
        }
        
        
        tables = new HashMap[count];
        parseTrees = new Tree[count]; 
        
        Tree fourthDiv = parseTree.root.Children.get(3);
        Tree sbody = fourthDiv.root.Children.get(0);
        
        tables[0] = cloneHashMap(table.list);
        parseTrees[0] = parseTree;
        currentTable = tables[0];
        
        //initializing global variables
        Tree vList = parseTree.root.Children.get(2).root.Children.get(2);
        if(vList.root.SymbolName.compareTo(Variable.vList.toString()) == 0){
            initialize(vList,0,0);
        }
        //setting up parseTree and table per scope
        
        //should be index 1 cause main scope
        parseTrees[mapping.get(sbody.root.Children.get(0).root.lexeme)] = sbody;
        tables[mapping.get(sbody.root.Children.get(0).root.lexeme)] = cloneHashMap(table.list.get("EXODUS").list);
        
        //should be index 2 onwards according to declaration in 2ndDiv
        if(fourthDiv.root.Children.size() == 2){
            Tree body = fourthDiv.root.Children.get(1);
            parseTrees[mapping.get(body.root.Children.get(0).root.lexeme)] = body;
            tables[mapping.get(body.root.Children.get(0).root.lexeme)] = cloneHashMap(table.list.get(body.root.Children.get(0).root.lexeme).list);
            while(body.root.Children.size() == 5){
                body = body.root.Children.get(4);
                tables[mapping.get(body.root.Children.get(0).root.lexeme)] = cloneHashMap(table.list.get(body.root.Children.get(0).root.lexeme).list);
                parseTrees[mapping.get(body.root.Children.get(0).root.lexeme)] = body;  
            }
        }
        //sanitizing big body tree into many small body trees
        for(int i = 2; i < parseTrees.length; i++){
            if(parseTrees[i].root.Children.size() == 5){
                parseTrees[i].root.Children.remove(4);
            }
        }
        Semantics.checkUndeclaredIds(parseTrees, tables);
    }
    
    
    public static HashMap<String, Identifier> cloneHashMap(HashMap<String, Identifier> temp){
        HashMap<String, Identifier> clone = new HashMap<>();
        if(temp != null){
            for(String key : temp.keySet()){
                Identifier tempId = temp.get(key);
                Identifier cloneId = new Identifier(new Token(Type.Id,tempId.name));
                cloneId.list = null;
                cloneId.isFinal = tempId.isFinal;
                cloneId.value = tempId.value;
                cloneId.datatype = tempId.datatype;
                clone.put(key, cloneId);
            }

            return clone;
        }
        else
            return null;
    }
    
    public void printScopes(){
        for(int i = 0; i < parseTrees.length; i++){
            System.out.println("changed scope : " + i);
            for(String key : tables[i].keySet()){
                 Identifier entry = tables[i].get(key);
                System.out.print("name : " + entry.name + "\t" );
                System.out.print("named constant : " + entry.isFinal + "\t");
                System.out.print("datatype : "  + entry.isDatatype() + "\t");
                System.out.println("value : " + entry.value);
                if(entry.list != null){
                    for(String localKey : entry.list.keySet()){
                        System.out.println("");
                        Identifier entryLocal = entry.list.get(localKey);
                        System.out.print("\t"  +"name : " + entryLocal.name + "\t" );
                        System.out.print("named constant : " + entryLocal.isFinal + "\t");
                        System.out.print("datatype : "  + entryLocal.isDatatype() + "\t");
                        System.out.print( "value : " + entryLocal.value);
                    }
                }
                System.out.println("");
            }
        }
    }
    
    public void Interpret() throws SemanticException{
        //init to main scope
        callStack.push(parseTrees[1]);
        tableStack.push(tables[1]);
        
        while(!callStack.isEmpty()){
            currentTree = callStack.pop();
            currentTable = tableStack.pop();
            traverseTree();
        }
        
    }
    
    public void traverseTree() throws SemanticException{
        while(currentTree != null && currentTree.root.Children.size() > 0){
//                System.out.println(currentTree.root.SymbolName);
                switch(currentTree.root.SymbolName){
                    case "Body" : 
                    case "sBody" : currentTree = currentTree.root.Children.get(2); break;
                    case "code" : 
                        if(currentTree.root.Children.get(2).root.SymbolName.compareTo(Variable.vList.toString()) == 0){
                            initialize(currentTree.root.Children.get(2),0 ,0);
                        }
                        if(currentTree.root.Children.get(currentTree.root.Children.size() - 1).root.SymbolName.compareTo(Variable.Main.toString()) == 0){
                            currentTree = currentTree.root.Children.get(currentTree.root.Children.size() - 1);
                        }
                        else{
                            currentTree = null;
                        }
                        break;
                    case "Main" : 
                        doTasks(currentTree.root.Children.get(0));
                        if(currentTree == null){
                            break;
                        }
                        else if(currentTree.root.Children.size() == 2){
                            currentTree = currentTree.root.Children.get(1);
                        }
                        else{
                            currentTree = null;
                        }
                        break;
                        
                }
                //traverse preorder
                //pop only when no more child to do
            }
    }
    
    public void doTasks(Tree t) throws SemanticException{
//        System.out.println("====DO TASKS====");
//        System.out.println("check for "+t.root.SymbolName);
        switch(t.root.SymbolName){
            case "Declare" : 
                update(t); break;
            case "IfStatement" : 
//                System.out.println(t);
                if(condition(t.root.Children.get(2).root.Children.get(0))){
                    doingMain(t.root.Children.get(5));
                }
                else if((!condition(t.root.Children.get(2).root.Children.get(0))) && t.root.Children.size() == 11  ){
                    doingMain(t.root.Children.get(9));
                }
                break;
            case "Get" : 
                get(t); break;
            case "Print" :
                print(t); break;
            case "MethodCall" :
                call(t); break;
            case "Loop" :
                loop(t); break;
        }            
    }
    
    
     public void print(Tree print){
        String val = print.root.Children.get(print.root.Children.size() - 3).root.Children.get(0).root.lexeme; //di ko ulit sure kung anong code yung para kunin niya yung token sa loob ng unveil
        if(val.charAt(0) != '\"' && val.charAt(val.lastIndexOf(val)) != '\"'){
            if(currentTable.containsKey(val)){
                val = currentTable.get(val).value;
            }
            else if(tables[0].containsKey(val)){
                val = tables[0].get(val).value;
            }
        }
        doPrint(val, print.root.Children.get(1).root.SymbolName);
        
        
    }
    
    public String doPrint(String val, String check){
        if(val.charAt(0) == '\"' && val.charAt(val.lastIndexOf(val)) == '\"'  || val.charAt(0) == '\'' && val.charAt(val.lastIndexOf(val)) == '\''){
            val = val.substring(1, val.length() -1);
        }
        
        if(check.compareTo(Type.UNVEIL.toString()) == 0){           
            System.out.println(val);
        }
        else{
            System.out.print(val);
        }
        return val;
    }
     
    public void get(Tree get) throws SemanticException{
        //do scanner methods depending on the tree while using the Id to determine the scanner function to use
        Scanner in = new Scanner(System.in);
        String id = get.root.Children.get(get.root.Children.size() - 3).root.lexeme;
        String tempVal;
        Identifier changing = null;
        if(currentTable.containsKey(id))
            changing = currentTable.get(id);
        else if(tables[0].containsKey(id))
            changing = tables[0].get(id);
        else
            System.out.println(id);
        
        tempVal = in.nextLine();
        tempVal = tempVal.trim();
        tempVal = Semantics.checkValueInput(tempVal, changing.datatype);
        updateValue(changing, tempVal);
        
//        System.out.println(currentTable.get(id).value);
//        System.out.println(tables[0].get(id).value);
    }
    
    public void updateValue(Identifier id, String val) throws SemanticException{
        if(id.isFinal == 1)
            throw new SemanticException("Changing a Named Identifier : " + id.name);
        Semantics.typeChecking(val, id.datatype, id.name);
        id.value = val;
        
//        System.out.println("Identifier : " + id.name + " has changed value to : " + val);
        
    }
    
    public void updateValueInit(Identifier id, String val) throws SemanticException{
        Semantics.typeChecking(val, id.datatype, id.name);
        id.value = val;
        
//        System.out.println("Identifier : " + id.name + " has changed value to : " + val);
        
    }
    
    public void doingMain(Tree t) throws SemanticException{
        while(t != null && t.root.Children.size() > 0){
            switch(t.root.SymbolName){
                case "Main" : 
                    doTasks(t.root.Children.get(0));
                    if(t == null){
                        break;
                    }
                    else if(t.root.Children.size() == 2){
                        t = t.root.Children.get(1);
                    }
                    else{
                        t = null;
                    }
                    break;
            }
        }
    }

    public void loop(Tree loop) throws SemanticException{
        //insert Id into currentTable
        Tree ctrvar = loop.root.Children.get(2); //3rd child is Id, insert to currentTable 
        Tree ctrval = loop.root.Children.get(4).root.Children.get(0); //5th child is its value in integer     
        Tree condiTree = loop.root.Children.get(6); //7th child is condition ctr == 5           
        Tree incTree = loop.root.Children.get(8); //9th child is increment statement.
   
        int ctr=0;
        if(ctrval.getTerminal().contains("NEG")){           
            String s = ctrval.root.Parent.Children.get(2).root.lexeme;
            try{
            //number
                ctr = -(Integer.parseInt(s));
                addToTable(ctrvar.root.lexeme,2,0,Integer.toString(ctr));
            }catch(NumberFormatException e){
                //identifier
                String ct;
                int dtype;
                if(currentTable.containsKey(s)){
                    ct = currentTable.get(s).value;
                    dtype = currentTable.get(s).datatype;
                }
                else if(tables[0].containsKey(s)){
                    ct = tables[0].get(s).value;
                    dtype = tables[0].get(s).datatype;
                }
                else
                    throw new SemanticException("Undeclared Id : " + ctrval.root.lexeme);
                if(dtype != 2)
                    throw new SemanticException("Id should be NUMBER :" + ctrval.root.lexeme);
                
                ctr = -(Integer.parseInt(ct));
                addToTable(ctrvar.root.lexeme,2,0,Integer.toString(ctr));
                
            } 
        }
        else{
            try{
            //number
                ctr = Integer.parseInt(ctrval.root.lexeme);
                addToTable(ctrvar.root.lexeme,2,0,ctrval.root.lexeme);
            }catch(NumberFormatException e){
                //identifier
                String ct;
                int dtype;
                if(currentTable.containsKey(ctrval.root.lexeme)){
                    ct = currentTable.get(ctrval.root.lexeme).value;
                    dtype = currentTable.get(ctrval.root.lexeme).datatype;
                }
                else if(tables[0].containsKey(ctrval.root.lexeme)){
                    ct = tables[0].get(ctrval.root.lexeme).value;
                    dtype = tables[0].get(ctrval.root.lexeme).datatype;
                }
                else
                    throw new SemanticException("Undeclared Id : " + ctrval.root.lexeme);
                if(dtype != 2)
                    throw new SemanticException("Id should be NUMBER :" + ctrval.root.lexeme);
                addToTable(ctrvar.root.lexeme,2,0,ct);
                ctr = Integer.parseInt(ct);
            } 
        }
        
                    

        int incval=1; //increment value
        if (incTree.getTerminal().contains("NEG")){
            //decrement, 3rd child is Num
            try{
                if(currentTable.containsKey(incTree.root.Children.get(2).root.lexeme)){
                    incval = -Integer.parseInt(currentTable.get(incTree.root.Children.get(2).root.lexeme).value);
                }
                else if(tables[0].containsKey(incTree.root.Children.get(2).root.lexeme)){
                    incval = -Integer.parseInt(tables[0].get(incTree.root.Children.get(2).root.lexeme).value);
                }
                else
                    incval = -(Integer.parseInt(incTree.root.Children.get(2).getTerminal().trim()));
            }
            catch(NumberFormatException e){
                throw new SemanticException(incTree.root.Children.get(2).root.lexeme +  " : is not NUMBER :");
            }
        }
        else if(incTree.root.Children.get(0).root.SymbolName.equals("Id")){
            try{
                if(currentTable.containsKey(incTree.root.Children.get(0).root.lexeme))
                    incval = Integer.parseInt(currentTable.get(incTree.root.Children.get(0).root.lexeme).value);
                else if(tables[0].containsKey(incTree.root.Children.get(0).root.lexeme))
                    incval = Integer.parseInt(tables[0].get(incTree.root.Children.get(0).root.lexeme).value);
                else
                    throw new SemanticException("Undeclared Id : " + incTree.root.Children.get(0).root.lexeme); 
            
            }
            catch(NumberFormatException e){
                throw new SemanticException(incTree.root.Children.get(0).root.lexeme + " : is not NUMBER");
            }
            
        }
        else {
            //increment, 1st child is Num
            incval = Integer.parseInt(incTree.root.Children.get(0).getTerminal().trim());
        }
        
        Identifier ctrId;
        if(currentTable.containsKey(ctrvar.root.lexeme))
            ctrId = currentTable.get(ctrvar.root.lexeme);
        else if(tables[0].containsKey(ctrvar.root.lexeme))
            ctrId = tables[0].get(ctrvar.root.lexeme);
        else
            throw new SemanticException(ctrvar.root.lexeme +" : is not declared in loop");
        
        Tree main = loop.root.Children.get(11); // main is 12th child
        ArrayList<Tree> mainKids = main.root.Children; //will always be 2 or 1
        
        while(condition(condiTree.root.Children.get(0))){
            for(Tree t: mainKids){
                if(t.root.SymbolName.equals("Main")){
                    doingMain(t);
                }
                else{//first child
                    doTasks(t);
                }
            }
                       
            ctr+=incval;
            updateValue(ctrId, Integer.toString(ctr));
//            addToTable(ctrvar.root.lexeme,2,0,Integer.toString(ctr));
        }
        
    }
    
    public int eval(Tree expression) throws SemanticException{
        switch(expression.root.SymbolName){
            case "+" : return eval(expression.root.Children.get(0)) + eval(expression.root.Children.get(1)); 
            case "*" : return eval(expression.root.Children.get(0)) * eval(expression.root.Children.get(1));
            case "-" : return eval(expression.root.Children.get(0)) - eval(expression.root.Children.get(1));
            case "OPneg" :
                return -eval(expression.root.Children.get(0)); 
            case "Id" : 
                try{
                    if(currentTable.containsKey(expression.root.lexeme)){
                        return Integer.parseInt(currentTable.get(expression.root.lexeme).value);
                    }
                    else if(tables[0].containsKey(expression.root.lexeme)){
                        return Integer.parseInt(tables[0].get(expression.root.lexeme).value);
                    }
                    else{
                        throw new SemanticException("Undeclared Id : " + expression.root.lexeme);
                    }
                }
                catch(NumberFormatException e){
                    throw new SemanticException(expression.root.lexeme + " is not NUMBER");
                }
            case "Num" : return Integer.parseInt(expression.root.lexeme);
            default : throw new SemanticException("Dunno to eval this : " +  expression.root.SymbolName);
        }
    }
    
    public boolean condition(Tree condition) throws SemanticException{
        // pass thee child of the condition, not the condition itself thank you
        Tree temp = condition;
        if (temp.root.Children.size() == 0){
            if ( !(temp.root.lexeme.equals(Type.GOOD.toString()) 
                    || temp.root.lexeme.equals(Type.BAD.toString()))){
                if(currentTable.containsKey(temp.root.lexeme)){
                    if (currentTable.get(temp.root.lexeme).value.equals(Type.GOOD.toString())){
                        //System.out.println("TRUE");
                        return true;
                    } else if (currentTable.get(temp.root.lexeme).value.equals(Type.BAD.toString())){
                        //System.out.println("FALSE");
                        return false;
                    } else {
                        throw new SemanticException(temp.root.lexeme + " is not a BOOLEAN datatype");
                    }
                }
                else if(tables[0].containsKey(temp.root.lexeme)){
                    if (tables[0].get(temp.root.lexeme).value.equals(Type.GOOD.toString())){
                        //System.out.println("TRUE");
                        return true;
                    } else if (tables[0].get(temp.root.lexeme).value.equals(Type.BAD.toString())){
                        //System.out.println("FALSE");
                        return false;
                    } else {
                        throw new SemanticException(temp.root.lexeme + " is not a BOOLEAN datatype");
                    }
                }
            } else {
                if (temp.root.lexeme.equals(Type.GOOD.toString())){
//                    System.out.println("TRUE");
                    return true;
                } else {
//                    System.out.println("FALSE");
                    return false;
                }
            }
        } 
        else {
            if (Semantics.isRelational(temp.root.lexeme)){

                //CHECK IF ID THEN LOOK FOR IT IN TABLE
                String value0 = temp.root.Children.get(0).root.lexeme;
                String value1 = temp.root.Children.get(1).root.lexeme;
//                System.out.println(value0 + " vs " + value1);
                if(currentTable.containsKey(temp.root.Children.get(0).root.lexeme)){
                   value0 = currentTable.get(temp.root.Children.get(0).root.lexeme).value;
                }
                else if(tables[0].containsKey(temp.root.Children.get(0).root.lexeme)){
                    value0 = tables[0].get(temp.root.Children.get(0).root.lexeme).value;
                }
                if(currentTable.containsKey(temp.root.Children.get(1).root.lexeme)){
                   value1 = currentTable.get(temp.root.Children.get(1).root.lexeme).value;
                }
                else if(tables[0].containsKey(temp.root.Children.get(1).root.lexeme)){
                    value1 = tables[0].get(temp.root.Children.get(1).root.lexeme).value;
                }
//                printScopes();
//                System.out.println(value0 + " vs " + value1);
                if (Semantics.isNumeric(value0) && Semantics.isNumeric(value1)){
                    switch (temp.root.lexeme){
                        case "<=": return Integer.parseInt(value0)<=Integer.parseInt(value1);
                        case "=": return Integer.parseInt(value0)==Integer.parseInt(value1);
                        case ">=": return Integer.parseInt(value0)>=Integer.parseInt(value1);
                        case "<":  return Integer.parseInt(value0)<Integer.parseInt(value1);
                        case ">": return Integer.parseInt(value0)>Integer.parseInt(value1);
                        case "!=": return Integer.parseInt(value0)!=Integer.parseInt(value1);
                    }
                } 
                else if (!Semantics.isNumeric(value0) && !Semantics.isNumeric(value1)){
                     switch (temp.root.lexeme){
                        case "=": if (value0.equals(value1))
                                    return true;
                                    else return false;
                        case "!=": if (!value0.equals(value1))
                                    return true;
                                    else return false;
                        default: throw new SemanticException(temp.root.lexeme + " is not allowed on non-NUMBER types");
                    }
                }
                else{
                    //catch for    numeric:  non-numeric
                    throw new SemanticException("Different Datatype");
                }
            } 
            else{
                switch (temp.root.lexeme){
                    case "AND": 
                        return (condition(temp.root.Children.get(0)) 
                                && condition(temp.root.Children.get(1)));
                    case "OR":
                        return (condition(temp.root.Children.get(0)) 
                                || condition(temp.root.Children.get(1)));
                    case "NAND": 
                        return !(condition(temp.root.Children.get(0)) 
                                && condition(temp.root.Children.get(1)));
                    case "NOR": 
                        return !(condition(temp.root.Children.get(0)) 
                                || condition(temp.root.Children.get(1)));
                    case "NOT": 
                        return !condition(temp.root.Children.get(0));
                }
            }
        }
        return false;
    }
    
    public void update(Tree declare) throws SemanticException{
        Identifier updating;
        int dtype;
        String id = declare.root.Children.get(0).root.lexeme;
        if(currentTable.containsKey(id))
            updating = currentTable.get(id);
        else if(tables[0].containsKey(id))
            updating = tables[0].get(id);
        else
            throw new SemanticException("Undeclared Id : " + id);
        if(declare.root.Children.get(2).root.SymbolName.compareTo(Variable.Mathematical.toString()) == 0){
            updateValue(updating, String.valueOf(eval(declare.root.Children.get(2).root.Children.get(0))));
        }
        else
            updateValue(updating, declare.root.Children.get(2).root.Children.get(0).root.lexeme);

    }
    
    public void call(Tree summon) throws SemanticException{
        Tree parent = null;
        if(currentTree.root.Children.size() >= 2){
            parent = currentTree.root.Children.get(1);
        }
        else{
            parent = null;
        }
//        System.out.println("currentTree address is " + currentTree);
//        System.out.println("temp tree address is " + parent);
        callStack.push(parent);
        tableStack.push(currentTable);
        currentTable = cloneHashMap(tables[mapping.get(summon.root.Children.get(1).root.lexeme)]);//reset the currentTable to compilation-phase table
        
        currentTree = parseTrees[mapping.get(summon.root.Children.get(1).root.lexeme)];
        traverseTree();
//        System.out.println("currentTree address is " + currentTree);
//        System.out.println("temp tree address is " + parent);
    
    }
    
    public void initialize(Tree vlist, int dtypeP, int isFinalP) throws SemanticException{
        //update symbol table with the corresponding values with type checking and if the value are Ids then check if exist; since we did not bind values on compile time
        //if an Id does not have a rhs e.g (NUMBER x), then initialize value to default values as stated in phase 1 revisions
        //use currentTable field

        int kids = vlist.root.Children.size();
        String var,val;
        int dtype=dtypeP,isFinal=isFinalP;
        boolean flag=false;
        //go through children
        for (int ctr = 0;ctr < kids;ctr++){
//            System.out.println(vlist.root.Children.get(ctr).root.SymbolName);
            if(vlist.root.Children.get(ctr).root.SymbolName.compareTo("CONFIRM")==0){
                isFinal = 1;            
                switch(vlist.root.Children.get(ctr+1).root.Children.get(0).root.lexeme){
                    case "STRING":  dtype = 0; flag=false;
                                    break;
                    case "NUMBER":  dtype = 2; flag=false;
                                    break;
                    case "CHARACTER": dtype = 1; flag=false;
                                    break;
                    case "BOOLEAN":  dtype = 3; flag=false;
                                    break;
                    default:flag=true;
                }
                
                var = vlist.root.Children.get(ctr+2).root.lexeme;
                if( (ctr + 4) < kids)
                    val = vlist.root.Children.get(ctr+4).root.Children.get(0).root.lexeme;
                else
                    val = null;
                addToTable(var,dtype,isFinal,val);
                ctr+=5;
//            System.out.println(vlist.root.Children.get(ctr).root.SymbolName);
            }
            else if(vlist.root.Children.get(ctr).root.SymbolName.compareTo("dataType")==0 || vlist.root.Children.get(ctr).root.SymbolName.compareTo(Type.Id.toString())==0){
                isFinal=0;
                if(vlist.root.Children.get(ctr).root.SymbolName.compareTo(Type.Id.toString()) == 0){
                    flag =true;
                }
                else{
                    switch(vlist.root.Children.get(ctr).root.Children.get(0).root.lexeme){
                            case "STRING":  dtype = 0;
                                            flag=false;
                                            break;
                            case "NUMBER":  dtype = 2; flag=false;
                                            break;
                            case "CHARACTER":  dtype = 1; flag=false;
                                            break;
                            case "BOOLEAN":  dtype = 3; flag=false;
                                            break;
                    } 
                }
                
                if(!flag){          //pag yung continuous something
                    var = vlist.root.Children.get(ctr+1).root.lexeme;
                    if( (ctr + 2) < kids && vlist.root.Children.get(ctr + 2).root.SymbolName.compareTo(Type.IS.toString()) == 0){
                        val = vlist.root.Children.get(ctr+3).root.Children.get(0).root.lexeme;
                        ctr+=4;
                    }
                    else{
                        val = null;
                        ctr+=2;
                    }
                }
                else{
                    var = vlist.root.Children.get(ctr).root.lexeme;
                    if((ctr + 1) < kids && vlist.root.Children.get(ctr + 1).root.SymbolName.compareTo(Type.IS.toString()) == 0){
                        val = vlist.root.Children.get(ctr+2).root.Children.get(0).root.lexeme;
                        ctr+=3;
                    }
                    else{
                        val = null;
                        ctr +=1;
                    }
                }
//                System.out.println(var + " this is going in");
                addToTable(var,dtype,isFinal,val);
            }
            else if(vlist.root.Children.get(ctr).root.SymbolName.compareTo("vList2")==0){ //recursion on vlist
                if(vlist.root.Children.get(ctr).root.Children.get(0).root.SymbolName.compareTo(Variable.vList.toString()) == 0){
                    initialize(vlist.root.Children.get(ctr).root.Children.get(0), 0, 0); 
                }
                else{//recursion on comma
                    initialize(vlist.root.Children.get(ctr), dtype, isFinal);
                }
                
            }
            
        }        
    }
   
       
    
    public void addToTable (String var, int dtype,int isFinal,String val) throws SemanticException{
        boolean tableInLocal = true;
        if(currentTable.containsKey(val)){
            val = currentTable.get(val).value;
        }
        else if(tables[0].containsKey(val)){            
            val = currentTable.get(val).value;
            tableInLocal = false;
        }
        if(val==null){
            switch(dtype){
                case 0: val="\"Jesus\"";
                        break;
                case 1: val="'┼'";
                        break;
                case 2: val="777";
                        break;
                case 3: val="GOOD";
                        break;
                }           
        }
        
        Semantics.typeChecking(val, dtype, var);
        
        Identifier newIden;
        if(tableInLocal){
            if(currentTable.get(var)==null){
                newIden = new Identifier(new Token(Type.Id,var));
            }else{
                newIden = currentTable.get(var);
            }
        }
        else{
            if(tables[0].get(var)==null){
                newIden = new Identifier(new Token(Type.Id,var));
            }else{
                newIden = tables[0].get(var);
            }
        }
        newIden.datatype = dtype;
        newIden.isFinal = isFinal;
        updateValueInit(newIden, val);
        if(tableInLocal)
            currentTable.put(var, newIden); //overwrite for purpose of loop
        else
            tables[0].put(var, newIden); 
        
//        System.out.println(newIden.name + " " + newIden.value);
    }

}