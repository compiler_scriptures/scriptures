package compiler;

import java.util.HashMap;

/**
 *
 * @author Josh
 */
public class ActionTable {
    public int state = 206;
    public HashMap<Type,String[]>[] row = new HashMap[state]; //30 not sure here haha
    
    ActionTable(){      
        //initialize so all rows have hashmap;
        for(int i = 0; i < state; i++){
            insert(i, Type.ERR, new String[]{""});
        }
        //start of josh
        insert(0, Type.CREATION, new String[]{"S","3"} );
        
        insert(1, Type.$, new String[]{"R", Variable.SPrime.toString(), Variable.Start.toString()});
        
        insert(2, Type.GENESIS, new String[]{"S", "5"});
        
        insert(3, Type.Id, new String[]{"S", "6"});
        
        insert(4, Type.BAPTIZE, new String[]{"S", "8"});
        
        insert(5, Type.LEFTSQ, new String[]{"S","10"});
        
        insert(6, Type.eos, new String []{"S", "9"});
        
        insert(7, Type.EXODUS, new String[]{"S", "69"});
        
        insert(8, Type.LEFTSQ, new String[]{"S", "19"});
        
        insert(9, Type.GENESIS, new String[]{"R", Variable.FirstDiv.toString(), Type.CREATION.toString(), Type.Id.toString(), Type.eos.toString()});
        
        insert(10, Type.Id, new String[]{"S", "11"});
        insert(10, Type.RIGHTSQ, new String[]{"S", "14"});
        
        insert(11, Type.RIGHTSQ, new String[]{"R", Variable.mList.toString() ,Type.Id.toString()});
        insert(11, Type.COMMA, new String[]{"S", "12"});
        
        insert(12, Type.Id, new String[]{"S","11"});
        
        insert(13, Type.RIGHTSQ, new String[]{"R",Variable.mList.toString() ,Type.Id.toString(), Type.COMMA.toString(), Variable.mList.toString()});
        
        insert(14, Type.eos, new String[]{"S", "15"});
        
        insert(15, Type.BAPTIZE, new String[]{"R", Variable.SecondDiv.toString(), Type.GENESIS.toString(), Type.LEFTSQ.toString(), Type.RIGHTSQ.toString(), Type.eos.toString()});
        
        insert(16, Type.RIGHTSQ, new String[]{"S", "17"});
        
        insert(17, Type.eos, new String[]{"S","18"});
        
        insert(18, Type.BAPTIZE, new String[]{"R", Variable.SecondDiv.toString(), Type.GENESIS.toString(), Type.LEFTSQ.toString(), Variable.mList.toString(), Type.RIGHTSQ.toString(), Type.eos.toString()});
        
        insert(19, Type.NUMBER, new String[]{"S", "23"});
        insert(19, Type.STRING, new String[]{"S","24"});
        insert(19, Type.BOOLEAN, new String[]{"S", "26"});
        insert(19, Type.CHARACTER, new String[]{"S","25"});
        insert(19, Type.RIGHTSQ, new String[]{"S","27"});
        insert(19, Type.CONFIRM, new String[]{"S", "56"});
        
        insert(20, Type.RIGHTSQ, new String[]{"S", "21"});
        
        insert(21, Type.eos, new String[]{"S","22"});
        
        insert(22, Type.EXODUS, new String[]{"R", Variable.ThirdDiv.toString(), Type.BAPTIZE.toString(), Type.LEFTSQ.toString(), Variable.vList.toString(), Type.RIGHTSQ.toString(), Type.eos.toString()});
        
        insert(23, Type.Id, new String[]{"R", Variable.dataType.toString(), Type.NUMBER.toString()});
        
        insert(24, Type.Id, new String[]{"R", Variable.dataType.toString(), Type.STRING.toString()});
        
        insert(25, Type.Id, new String[]{"R", Variable.dataType.toString(), Type.CHARACTER.toString()});
        
        insert(26, Type.Id, new String[]{"R", Variable.dataType.toString(), Type.BOOLEAN.toString()});
        
        insert(27, Type.eos, new String[]{"S", "28"});
        
        insert(28, Type.EXODUS, new String[]{"R", Variable.ThirdDiv.toString(), Type.BAPTIZE.toString(), Type.LEFTSQ.toString(), Type.RIGHTSQ.toString(), Type.eos.toString()});
        
        insert(29, Type.Id, new String[]{"S", "30"});
        
        insert(30, Type.IS, new String[]{"S", "31"});
        insert(30, Type.COMMA, new String[]{"S","37"});
        insert(30, Type.RIGHTSQ, new String[]{"R", Variable.vList.toString(), Variable.dataType.toString(), Type.Id.toString()});
        
        insert(31, Type.Char, new String[]{"S","33"});
        insert(31, Type.Num, new String[]{"S","32"});
        insert(31, Type.Literal, new String[]{"S","34"});
        insert(31, Type.Id, new String[]{"S","36"});
        insert(31, Type.Bool, new String[]{"S","35"});
        insert(31, Type.OPneg, new String[]{"S","44"});
        
        insert(32, Type.RELOPgt, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        insert(32, Type.RELOPge, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        insert(32, Type.RELOPlt, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        insert(32, Type.RELOPle, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        insert(32, Type.RELOPeq, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        insert(32, Type.RELOPne, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        insert(32, Type.LOGOPand, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        insert(32, Type.LOGOPor, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        insert(32, Type.LOGOPnand, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        insert(32, Type.LOGOPnor, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        insert(32, Type.BY, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        insert(32, Type.COMMA, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        insert(32, Type.RIGHTSQ, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        insert(32, Type.eos, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        insert(32, Type.RIGHTP, new String[]{"R",Variable.dataVal.toString(), Type.Num.toString()});
        
        insert(33, Type.RELOPgt, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        insert(33, Type.RELOPge, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        insert(33, Type.RELOPlt, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        insert(33, Type.RELOPle, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        insert(33, Type.RELOPeq, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        insert(33, Type.RELOPne, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        insert(33, Type.LOGOPand, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        insert(33, Type.LOGOPor, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        insert(33, Type.LOGOPnand, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        insert(33, Type.LOGOPnor, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        insert(33, Type.BY, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        insert(33, Type.COMMA, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        insert(33, Type.RIGHTSQ, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        insert(33, Type.eos, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        insert(33, Type.RIGHTP, new String[]{"R",Variable.dataVal.toString(), Type.Char.toString()});
        
        insert(34, Type.RELOPgt, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        insert(34, Type.RELOPge, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        insert(34, Type.RELOPlt, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        insert(34, Type.RELOPle, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        insert(34, Type.RELOPeq, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        insert(34, Type.RELOPne, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        insert(34, Type.LOGOPand, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        insert(34, Type.LOGOPor, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        insert(34, Type.LOGOPnand, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        insert(34, Type.LOGOPnor, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        insert(34, Type.BY, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        insert(34, Type.COMMA, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        insert(34, Type.RIGHTSQ, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        insert(34, Type.eos, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        insert(34, Type.RIGHTP, new String[]{"R",Variable.dataVal.toString(), Type.Literal.toString()});
        
        insert(35, Type.RELOPgt, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        insert(35, Type.RELOPge, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        insert(35, Type.RELOPlt, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        insert(35, Type.RELOPle, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        insert(35, Type.RELOPeq, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        insert(35, Type.RELOPne, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        insert(35, Type.LOGOPand, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        insert(35, Type.LOGOPor, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        insert(35, Type.LOGOPnand, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        insert(35, Type.LOGOPnor, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        insert(35, Type.BY, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        insert(35, Type.COMMA, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        insert(35, Type.RIGHTSQ, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        insert(35, Type.eos, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        insert(35, Type.RIGHTP, new String[]{"R",Variable.dataVal.toString(), Type.Bool.toString()});
        
        insert(36, Type.RELOPgt, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        insert(36, Type.RELOPge, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        insert(36, Type.RELOPlt, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        insert(36, Type.RELOPle, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        insert(36, Type.RELOPeq, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        insert(36, Type.RELOPne, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        insert(36, Type.LOGOPand, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        insert(36, Type.LOGOPor, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        insert(36, Type.LOGOPnand, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        insert(36, Type.LOGOPnor, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        insert(36, Type.BY, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        insert(36, Type.COMMA, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        insert(36, Type.RIGHTSQ, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        insert(36, Type.eos, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        insert(36, Type.RIGHTP, new String[]{"R",Variable.dataVal.toString(), Type.Id.toString()});
        
        insert(37, Type.Id, new String[]{"S", "38"});
        insert(37, Type.NUMBER, new String[]{"S", "23"});
        insert(37, Type.STRING, new String[]{"S", "24"});
        insert(37, Type.BOOLEAN, new String[]{"S", "26"});
        insert(37, Type.CHARACTER, new String[]{"S","25"});
        insert(37, Type.CONFIRM, new String[]{"S","56"});
        
        insert(38, Type.IS, new String[]{"S","50"});
        insert(38, Type.RIGHTSQ, new String[]{"R",Variable.vList2.toString(), Type.Id.toString()});
        insert(38, Type.COMMA, new String[]{"S","51"});
        
        insert(39, Type.RIGHTSQ, new String[]{"R",Variable.vList.toString(), Variable.dataType.toString(), Type.Id.toString(), Type.IS.toString(), Variable.dataVal.toString()});
        insert(39, Type.COMMA, new String[]{"S","40"});
        
        insert(40, Type.Id, new String[]{"S", "38"});
        insert(40, Type.NUMBER, new String[]{"S", "23"});
        insert(40, Type.STRING, new String[]{"S", "24"});
        insert(40, Type.BOOLEAN, new String[]{"S", "26"});
        insert(40, Type.CHARACTER, new String[]{"S","25"});
        insert(40, Type.CONFIRM, new String[]{"S","56"});
        
        insert(41, Type.RIGHTSQ, new String[]{"R", Variable.vList.toString(), Variable.dataType.toString(), Type.Id.toString(),Type.COMMA.toString(), Variable.vList2.toString()});
        
        insert(42, Type.RIGHTSQ, new String[]{"R", Variable.vList2.toString(), Variable.vList.toString()});
        
        insert(43, Type.RIGHTSQ, new String[]{"R", Variable.vList.toString(),Variable.dataType.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataVal.toString(),Type.COMMA.toString(), Variable.vList2.toString()});
        
        insert(44, Type.LEFTP, new String[]{"S","45"});
        
        insert(45, Type.Num, new String[]{"S","46"});
        insert(45, Type.Id, new String[]{"S","47"});
        
        insert(46, Type.RIGHTP, new String[]{"S","48"});
        
        insert(47, Type.RIGHTP, new String[]{"S","49"});
        
        insert(48, Type.RIGHTSQ, new String[]{"R",Variable.dataVal.toString(), Type.OPneg.toString(), Type.LEFTP.toString(), Type.Num.toString(), Type.RIGHTP.toString()});
        insert(48, Type.COMMA, new String[]{"R",Variable.dataVal.toString(), Type.OPneg.toString(), Type.LEFTP.toString(), Type.Num.toString(), Type.RIGHTP.toString()});
        
        insert(49, Type.RIGHTSQ, new String[]{"R",Variable.dataVal.toString(), Type.OPneg.toString(), Type.LEFTP.toString(), Type.Id.toString(), Type.RIGHTP.toString()});
        insert(49, Type.COMMA, new String[]{"R",Variable.dataVal.toString(), Type.OPneg.toString(), Type.LEFTP.toString(), Type.Id.toString(), Type.RIGHTP.toString()});
        
        insert(50, Type.Char, new String[]{"S", "33"});
        insert(50, Type.Num, new String[]{"S", "32"});
        insert(50, Type.Literal, new String[]{"S", "34"});
        insert(50, Type.Id, new String[]{"S", "36"});
        insert(50, Type.Bool, new String[]{"S", "35"});
        insert(50, Type.OPneg, new String[]{"S", "44"});
        
        insert(51, Type.Id, new String[]{"S", "38"});
        insert(51, Type.NUMBER, new String[]{"S", "23"});
        insert(51, Type.STRING, new String[]{"S", "24"});
        insert(51, Type.BOOLEAN, new String[]{"S", "26"});
        insert(51, Type.CHARACTER, new String[]{"S", "25"});
        insert(51, Type.CONFIRM, new String[]{"S", "56"});
        
        insert(52, Type.RIGHTSQ, new String[]{"R",Variable.vList2.toString(),Type.Id.toString(),Type.COMMA.toString(),Variable.vList2.toString()});
        
        insert(53, Type.RIGHTSQ, new String[]{"R",Variable.vList2.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataVal.toString()});
        insert(53, Type.COMMA, new String[]{"S","54"});
        
        insert(54, Type.Id, new String[]{"S", "38"});
        insert(54, Type.NUMBER, new String[]{"S", "23"});
        insert(54, Type.STRING, new String[]{"S", "24"});
        insert(54, Type.BOOLEAN, new String[]{"S", "26"});
        insert(54, Type.CHARACTER, new String[]{"S","25"});
        insert(54, Type.CONFIRM, new String[]{"S","56"});
        
        insert(55, Type.RIGHTSQ, new String[]{"R",Variable.vList2.toString(), Type.Id.toString(), Type.IS.toString(), Variable.dataVal.toString(), Type.COMMA.toString(), Variable.vList2.toString()});
        
        insert(56, Type.NUMBER, new String[]{"S", "23"});
        insert(56, Type.STRING, new String[]{"S", "24"});
        insert(56, Type.BOOLEAN, new String[]{"S", "26"});
        insert(56, Type.CHARACTER, new String[]{"S","25"});
        
        insert(57, Type.Id, new String[]{"S","58"});
        
        insert(58, Type.IS, new String[]{"S","59"});
        
        insert(59, Type.Char, new String[]{"S", "33"});
        insert(59, Type.Num, new String[]{"S", "32"});
        insert(59, Type.Literal, new String[]{"S", "34"});
        insert(59, Type.Id, new String[]{"S", "36"});
        insert(59, Type.Bool, new String[]{"S", "35"});
        insert(59, Type.OPneg, new String[]{"S", "44"});
        
        insert(60, Type.RIGHTSQ, new String[]{"R", Variable.vList.toString(), Type.CONFIRM.toString(), Variable.dataType.toString(), Type.Id.toString(), Type.IS.toString(), Variable.dataVal.toString()});
        insert(60, Type.COMMA, new String[]{"S", "61"});
        
        insert(61, Type.Id, new String[]{"S", "38"});
        insert(61, Type.NUMBER, new String[]{"S", "23"});
        insert(61, Type.STRING, new String[]{"S", "24"});
        insert(61, Type.BOOLEAN, new String[]{"S", "26"});
        insert(61, Type.CHARACTER, new String[]{"S", "25"});
        insert(61, Type.CONFIRM, new String[]{"S", "56"});
        
        insert(62, Type.RIGHTSQ, new String[]{"R", Variable.vList.toString(), Type.CONFIRM.toString(), Variable.dataType.toString(), Type.Id.toString(), Type.IS.toString(), Variable.dataVal.toString(), Type.COMMA.toString(), Variable.vList2.toString()});
        
        insert(63, Type.REVELATION, new String[]{"S","65"});
        
        insert(64, Type.$, new String[]{"R",Variable.Start.toString(), Variable.FirstDiv.toString(), Variable.SecondDiv.toString(), Variable.ThirdDiv.toString(), Variable.FourthDiv.toString(), Variable.FifthDiv.toString()});
        
        insert(65, Type.eos, new String[]{"S","66"});
        
        insert(66, Type.$, new String[]{"R",Variable.FifthDiv.toString(), Type.REVELATION.toString(), Type.eos.toString()});
        
        insert(67, Type.REVELATION, new String[]{"R", Variable.FourthDiv.toString(), Variable.sBody.toString()});
        insert(67, Type.Id, new String[]{"S","106"});
        
        insert(68, Type.REVELATION, new String[]{"R", Variable.FourthDiv.toString(), Variable.sBody.toString(), Variable.Body.toString()});
        
        insert(69, Type.LEFTCU, new String[]{"S","70"});
        
        insert(70, Type.BAPTIZE, new String[]{"S","71"});
        
        insert(71,Type.RESURRECT,new String[]{"S","137"});
        insert(71,Type.LEFTSQ,new String[]{"S","72"});
        
        insert(72,Type.NUMBER,new String[]{"S","23"});
        insert(72,Type.STRING,new String[]{"S","24"});
        insert(72,Type.BOOLEAN,new String[]{"S","26"});
        insert(72,Type.CHARACTER,new String[]{"S","25"});
        insert(72,Type.RIGHTSQ,new String[]{"S","73"});
        insert(72,Type.CONFIRM,new String[]{"S","56"});
        
        insert(73,Type.eos,new String[]{"S","74"});
        
        insert(74,Type.IF,new String[]{"S","99"});
        insert(74,Type.PRAY,new String[]{"S","78"});
        insert(74,Type.UNVEIL,new String[]{"S","88"});
        insert(74,Type.Id,new String[]{"S","98"});
        insert(74,Type.RIGHTCU,new String[]{"R",Variable.code.toString(),Type.BAPTIZE.toString(),Type.LEFTSQ.toString(),Type.RIGHTSQ.toString(),Type.eos.toString()});
        insert(74,Type.SUMMON,new String[]{"S","134"});
        insert(74,Type.RESURRECT,new String[]{"S","137"});
        
        insert(75,Type.RIGHTSQ,new String[]{"S","76"});
        
        insert(76,Type.eos,new String[]{"S","192"});
        
        insert(77,Type.RIGHTCU,new String[]{"R",Variable.code.toString(),Type.BAPTIZE.toString(),Type.LEFTSQ.toString(),Variable.vList.toString(),Type.RIGHTSQ.toString(),Type.eos.toString(),Variable.Main.toString()});
        
        insert(78,Type.LEFTP,new String[]{"S","79"});
        insert(78,Type.FOR,new String[]{"S","83"});
        
        insert(79,Type.Id,new String[]{"S","80"});
        
        insert(80,Type.RIGHTP,new String[]{"S","81"});
        
        insert(81,Type.eos,new String[]{"S","82"});
        
        insert(82,Type.IF,new String[]{"R",Variable.Get.toString(),Type.PRAY.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(82,Type.RESURRECT,new String[]{"R",Variable.Get.toString(),Type.PRAY.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(82,Type.PRAY,new String[]{"R",Variable.Get.toString(),Type.PRAY.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(82,Type.UNVEIL,new String[]{"R",Variable.Get.toString(),Type.PRAY.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(82,Type.Id,new String[]{"R",Variable.Get.toString(),Type.PRAY.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(82,Type.RIGHTCU,new String[]{"R",Variable.Get.toString(),Type.PRAY.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(82,Type.SUMMON,new String[]{"R",Variable.Get.toString(),Type.PRAY.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        
        insert(83,Type.LEFTP,new String[]{"S","84"});
        
        insert(84,Type.Id,new String[]{"S","85"});
        
        insert(85,Type.RIGHTP,new String[]{"S","86"});
        
        insert(86,Type.eos,new String[]{"S","87"});
        
        insert(87,Type.SUMMON,new String[]{"R",Variable.Get.toString(),Type.PRAY.toString(),Type.FOR.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(87,Type.UNVEIL,new String[]{"R",Variable.Get.toString(),Type.PRAY.toString(),Type.FOR.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(87,Type.PRAY,new String[]{"R",Variable.Get.toString(),Type.PRAY.toString(),Type.FOR.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(87,Type.RESURRECT,new String[]{"R",Variable.Get.toString(),Type.PRAY.toString(),Type.FOR.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(87,Type.Id,new String[]{"R",Variable.Get.toString(),Type.PRAY.toString(),Type.FOR.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(87,Type.IF,new String[]{"R",Variable.Get.toString(),Type.PRAY.toString(),Type.FOR.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(87,Type.RIGHTCU,new String[]{"R",Variable.Get.toString(),Type.PRAY.toString(),Type.FOR.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        
        insert(88,Type.UNVEIL,new String[]{"S","117"});
        insert(88,Type.LEFTP,new String[]{"S","89"});
        insert(88,Type.THE,new String[]{"S","111"});
        
        insert(89,Type.Literal,new String[]{"S","103"});
        insert(89,Type.Id,new String[]{"S","105"});
        
        insert(90,Type.RIGHTCU,new String[]{"R",Variable.Main.toString(),Variable.Declare.toString()});
        insert(90,Type.RIGHTP,new String[]{"S","101"});
        
        insert(91,Type.RIGHTCU,new String[]{"R",Variable.code.toString(),Type.BAPTIZE.toString(),Type.LEFTSQ.toString(),Type.RIGHTSQ.toString(),Type.eos.toString(),Variable.Main.toString()});
        
        insert(92,Type.IF,new String[]{"S","99"});
        insert(92,Type.RESURRECT,new String[]{"S","137"});
        insert(92,Type.PRAY,new String[]{"S","78"});
        insert(92,Type.UNVEIL,new String[]{"S","88"});
        insert(92,Type.Id,new String[]{"S","98"});
        insert(92,Type.SUMMON,new String[]{"S","134"});
        insert(92, Type.RIGHTCU, new String[]{"R", Variable.Main.toString(), Variable.Declare.toString()});
        
        insert(93,Type.IF,new String[]{"S","99"});
        insert(93,Type.RESURRECT,new String[]{"S","137"});
        insert(93,Type.PRAY,new String[]{"S","78"});
        insert(93,Type.UNVEIL,new String[]{"S","88"});
        insert(93,Type.Id,new String[]{"S","98"});
        insert(93,Type.SUMMON,new String[]{"S","134"});
        insert(93, Type.RIGHTCU, new String[]{"R", Variable.Main.toString(), Variable.IfStatement.toString()});
        
        insert(94,Type.IF,new String[]{"S","99"});
        insert(94,Type.RESURRECT,new String[]{"S","137"});
        insert(94,Type.PRAY,new String[]{"S","78"});
        insert(94,Type.UNVEIL,new String[]{"S","88"});
        insert(94,Type.Id,new String[]{"S","98"});
        insert(94,Type.SUMMON,new String[]{"S","134"});
        insert(94, Type.RIGHTCU, new String[]{"R", Variable.Main.toString(), Variable.Get.toString()});
        
        insert(95,Type.IF,new String[]{"S","99"});
        insert(95,Type.RESURRECT,new String[]{"S","137"});
        insert(95,Type.PRAY,new String[]{"S","78"});
        insert(95,Type.UNVEIL,new String[]{"S","88"});
        insert(95,Type.Id,new String[]{"S","98"});
        insert(95,Type.SUMMON,new String[]{"S","134"});
        insert(95, Type.RIGHTCU, new String[]{"R", Variable.Main.toString(), Variable.Print.toString()});
        
        insert(96,Type.IF,new String[]{"S","99"});
        insert(96,Type.RESURRECT,new String[]{"S","137"});
        insert(96,Type.PRAY,new String[]{"S","78"});
        insert(96,Type.UNVEIL,new String[]{"S","88"});
        insert(96,Type.Id,new String[]{"S","98"});
        insert(96,Type.SUMMON,new String[]{"S","134"});
        insert(96, Type.RIGHTCU, new String[]{"R", Variable.Main.toString(), Variable.MethodCall.toString()});
        
        insert(97,Type.IF,new String[]{"S","99"});
        insert(97,Type.RESURRECT,new String[]{"S","137"});
        insert(97,Type.PRAY,new String[]{"S","78"});
        insert(97,Type.UNVEIL,new String[]{"S","88"});
        insert(97,Type.Id,new String[]{"S","98"});
        insert(97,Type.SUMMON,new String[]{"S","134"});
        insert(97, Type.RIGHTCU, new String[]{"R", Variable.Main.toString(), Variable.Loop.toString()});
        
        insert(98,Type.IS,new String[]{"S","175"});
        
        insert(99,Type.LEFTP,new String[]{"S","133"});
        
        insert(100,Type.RIGHTCU,new String[]{"R",Variable.Main.toString(),Variable.Declare.toString(),Variable.Main.toString()});
        
        insert(101,Type.eos,new String[]{"S","102"});
        
        insert(102,Type.IF,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(102,Type.RESURRECT,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(102,Type.PRAY,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(102,Type.UNVEIL,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(102,Type.Id,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(102,Type.RIGHTCU,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(102,Type.SUMMON,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        
        insert(103,Type.RIGHTP,new String[]{"R",Variable.dataValLiteral.toString(),Type.Literal.toString()});
        
        insert(104,Type.RIGHTCU,new String[]{"S","116"});
        
        insert(105,Type.RIGHTP,new String[]{"R",Variable.dataValLiteral.toString(),Type.Id.toString()});
        
        insert(106,Type.LEFTCU,new String[]{"S","107"});
        
        insert(107,Type.BAPTIZE,new String[]{"S","71"});
        
        insert(108,Type.RIGHTCU,new String[]{"S","109"});
        
        insert(109,Type.REVELATION,new String[]{"R",Variable.Body.toString(),Type.Id.toString(),Type.LEFTCU.toString(),Variable.code.toString(),Type.RIGHTCU.toString()});
        insert(109,Type.Id,new String[]{"S","106"});
        
        insert(110,Type.REVELATION,new String[]{"R",Variable.Body.toString(),Type.Id.toString(),Type.LEFTCU.toString(),Variable.code.toString(),Type.RIGHTCU.toString(),Variable.Body.toString()});
        
        insert(111,Type.LEFTP,new String[]{"S","112"});
        
        insert(112,Type.Literal,new String[]{"S","103"});
        insert(112,Type.Id,new String[]{"S","105"});
        
        insert(113,Type.RIGHTP,new String[]{"S","114"});
        
        insert(114,Type.eos,new String[]{"S","115"});
        
        insert(115,Type.IF,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.THE.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(115,Type.RESURRECT,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.THE.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(115,Type.PRAY,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.THE.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(115,Type.UNVEIL,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.THE.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(115,Type.Id,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.THE.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(115,Type.RIGHTCU,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.THE.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(115,Type.SUMMON,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.THE.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        
        insert(116,Type.REVELATION,new String[]{"R",Variable.sBody.toString(),Type.EXODUS.toString(),Type.LEFTCU.toString(),Variable.code.toString(),Type.RIGHTCU.toString()});
        insert(116,Type.Id,new String[]{"R",Variable.sBody.toString(),Type.EXODUS.toString(),Type.LEFTCU.toString(),Variable.code.toString(),Type.RIGHTCU.toString()});
        
        insert(117,Type.LEFTP,new String[]{"S","123"});
        insert(117,Type.THE,new String[]{"S","118"});
        
        insert(118,Type.LEFTP,new String[]{"S","119"});
        
        insert(119,Type.Literal,new String[]{"S","103"});
        insert(119,Type.Id,new String[]{"S","105"});
        
        insert(120,Type.RIGHTP,new String[]{"S","121"});
        
        insert(121,Type.eos,new String[]{"S","122"});
        
        insert(122,Type.IF,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.UNVEIL.toString(),Type.THE.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(122,Type.RESURRECT,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.UNVEIL.toString(),Type.THE.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(122,Type.PRAY,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.UNVEIL.toString(),Type.THE.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(122,Type.UNVEIL,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.UNVEIL.toString(),Type.THE.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(122,Type.Id,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.UNVEIL.toString(),Type.THE.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(122,Type.RIGHTCU,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.UNVEIL.toString(),Type.THE.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(122,Type.SUMMON,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.UNVEIL.toString(),Type.THE.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        
        insert(123,Type.Literal,new String[]{"S","103"});
        insert(123,Type.Id,new String[]{"S","105"});
        
        insert(124,Type.RIGHTP,new String[]{"S","125"});
        
        insert(125,Type.eos,new String[]{"S","126"});
        
        insert(126,Type.IF,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.UNVEIL.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(126,Type.RESURRECT,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.UNVEIL.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(126,Type.PRAY,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.UNVEIL.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(126,Type.UNVEIL,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.UNVEIL.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(126,Type.Id,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.UNVEIL.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(126,Type.RIGHTCU,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.UNVEIL.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        insert(126,Type.SUMMON,new String[]{"R",Variable.Print.toString(),Type.UNVEIL.toString(),Type.UNVEIL.toString(),Type.LEFTP.toString(),Variable.dataValLiteral.toString(),Type.RIGHTP.toString(),Type.eos.toString()});
        
        insert(127,Type.RIGHTCU,new String[]{"R",Variable.Main.toString(),Variable.IfStatement.toString(),Variable.Main.toString()});
        
        insert(128,Type.RIGHTCU,new String[]{"R",Variable.Main.toString(),Variable.Get.toString(),Variable.Main.toString()});
        
        insert(129,Type.RIGHTCU,new String[]{"R",Variable.Main.toString(),Variable.Print.toString(),Variable.Main.toString()});
        
        insert(130,Type.RIGHTCU,new String[]{"R",Variable.Main.toString(),Variable.MethodCall.toString(),Variable.Main.toString()});
        
        insert(131,Type.RIGHTCU,new String[]{"R",Variable.Main.toString(),Variable.Loop.toString(),Variable.Main.toString()});
        
        //removed 132. Skipped state
        
        insert(133,Type.Char,new String[]{"S","33"});
        insert(133,Type.Num,new String[]{"S","32"});
        insert(133,Type.Literal,new String[]{"S","34"});
        insert(133,Type.Id,new String[]{"S","36"});
        insert(133,Type.Bool,new String[]{"S","35"});
        insert(133,Type.OPneg,new String[]{"S","44"});
        insert(133,Type.LOGOPnot,new String[]{"S","154"});
        insert(133,Type.LEFTP,new String[]{"S","153"});
        
        insert(134,Type.Id,new String[]{"S","135"});
        
        insert(135,Type.eos,new String[]{"S","136"});
        
        insert(136,Type.IF,new String[]{"R",Variable.MethodCall.toString(),Type.SUMMON.toString(),Type.Id.toString(),Type.eos.toString()});
        insert(136,Type.RESURRECT,new String[]{"R",Variable.MethodCall.toString(),Type.SUMMON.toString(),Type.Id.toString(),Type.eos.toString()});
        insert(136,Type.PRAY,new String[]{"R",Variable.MethodCall.toString(),Type.SUMMON.toString(),Type.Id.toString(),Type.eos.toString()});
        insert(136,Type.UNVEIL,new String[]{"R",Variable.MethodCall.toString(),Type.SUMMON.toString(),Type.Id.toString(),Type.eos.toString()});
        insert(136,Type.Id,new String[]{"R",Variable.MethodCall.toString(),Type.SUMMON.toString(),Type.Id.toString(),Type.eos.toString()});
        insert(136,Type.RIGHTCU,new String[]{"R",Variable.MethodCall.toString(),Type.SUMMON.toString(),Type.Id.toString(),Type.eos.toString()});
        insert(136,Type.SUMMON,new String[]{"R",Variable.MethodCall.toString(),Type.SUMMON.toString(),Type.Id.toString(),Type.eos.toString()});
        
        insert(137,Type.LEFTP,new String[]{"S","138"});
        
        insert(138,Type.Id,new String[]{"S","139"});
        
        insert(139,Type.IS,new String[]{"S","140"});
        
        insert(140,Type.Num,new String[]{"S","142"});
        insert(140,Type.Id,new String[]{"S","141"});
        insert(140,Type.OPneg,new String[]{"S","143"});
        
      insert(141,Type.OPplus,new String[]{"R",Variable.dataValNum.toString(),Type.Id.toString()});  
      insert(141,Type.OPminus,new String[]{"R",Variable.dataValNum.toString(),Type.Id.toString()});  
      insert(141,Type.OPtimes,new String[]{"R",Variable.dataValNum.toString(),Type.Id.toString()});  
      insert(141,Type.eos,new String[]{"R",Variable.dataValNum.toString(),Type.Id.toString()});  
      insert(141,Type.RIGHTP,new String[]{"R",Variable.dataValNum.toString(),Type.Id.toString()});  
      insert(141,Type.UNTIL,new String[]{"R",Variable.dataValNum.toString(),Type.Id.toString()});  
      
      insert(142,Type.OPplus,new String[]{"R",Variable.dataValNum.toString(),Type.Num.toString()});  
      insert(142,Type.OPminus,new String[]{"R",Variable.dataValNum.toString(),Type.Num.toString()});  
      insert(142,Type.OPtimes,new String[]{"R",Variable.dataValNum.toString(),Type.Num.toString()});  
      insert(142,Type.eos,new String[]{"R",Variable.dataValNum.toString(),Type.Num.toString()});  
      insert(142,Type.RIGHTP,new String[]{"R",Variable.dataValNum.toString(),Type.Num.toString()});  
      insert(142,Type.UNTIL,new String[]{"R",Variable.dataValNum.toString(),Type.Num.toString()});  
      
      insert(143,Type.LEFTP,new String[]{"S","144"});  
      
      insert(144,Type.Num,new String[]{"S","147"});  
      insert(144,Type.Id,new String[]{"S","145"});  
      
      insert(145,Type.RIGHTP,new String[]{"S","146"});  
      
      insert(146,Type.OPplus,new String[]{"R",Variable.dataValNum.toString(),Type.OPneg.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString()});  
      insert(146,Type.OPminus,new String[]{"R",Variable.dataValNum.toString(),Type.OPneg.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString()});  
      insert(146,Type.OPtimes,new String[]{"R",Variable.dataValNum.toString(),Type.OPneg.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString()});  
      insert(146,Type.eos,new String[]{"R",Variable.dataValNum.toString(),Type.OPneg.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString()});  
      insert(146,Type.RIGHTP,new String[]{"R",Variable.dataValNum.toString(),Type.OPneg.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString()});  
      insert(146,Type.UNTIL,new String[]{"R",Variable.dataValNum.toString(),Type.OPneg.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.RIGHTP.toString()});  
      
      insert(147,Type.RIGHTP,new String[]{"S","148"});  
      
      insert(148,Type.OPplus,new String[]{"R",Variable.dataValNum.toString(),Type.OPneg.toString(),Type.LEFTP.toString(),Type.Num.toString(),Type.RIGHTP.toString()});  
      insert(148,Type.OPminus,new String[]{"R",Variable.dataValNum.toString(),Type.OPneg.toString(),Type.LEFTP.toString(),Type.Num.toString(),Type.RIGHTP.toString()});  
      insert(148,Type.OPtimes,new String[]{"R",Variable.dataValNum.toString(),Type.OPneg.toString(),Type.LEFTP.toString(),Type.Num.toString(),Type.RIGHTP.toString()});  
      insert(148,Type.eos,new String[]{"R",Variable.dataValNum.toString(),Type.OPneg.toString(),Type.LEFTP.toString(),Type.Num.toString(),Type.RIGHTP.toString()});  
      insert(148,Type.RIGHTP,new String[]{"R",Variable.dataValNum.toString(),Type.OPneg.toString(),Type.LEFTP.toString(),Type.Num.toString(),Type.RIGHTP.toString()});  
      insert(148,Type.UNTIL,new String[]{"R",Variable.dataValNum.toString(),Type.OPneg.toString(),Type.LEFTP.toString(),Type.Num.toString(),Type.RIGHTP.toString()});  
      
      insert(149,Type.LOGOPand,new String[]{"S","166"});  
      insert(149,Type.LOGOPor,new String[]{"S","167"});  
      insert(149,Type.LOGOPnand,new String[]{"S","168"});  
      insert(149,Type.LOGOPnor,new String[]{"S","169"});  
      insert(149,Type.RIGHTP,new String[]{"S","150"});  
      
      insert(150,Type.LEFTCU,new String[]{"S","151"});  
      
      insert(151,Type.IF,new String[]{"S","99"});  
      insert(151,Type.PRAY,new String[]{"S","78"});  
      insert(151,Type.UNVEIL,new String[]{"S","88"});  
      insert(151,Type.Id,new String[]{"S","98"});  
      insert(151,Type.SUMMON,new String[]{"S","134"});  
      insert(151, Type.RESURRECT, new String[]{"S","137"});
      
      insert(152,Type.RIGHTCU,new String[]{"S","160"});  
      
      insert(153,Type.Char,new String[]{"S","33"});  
      insert(153,Type.Num,new String[]{"S","32"});  
      insert(153,Type.Literal,new String[]{"S","34"});  
      insert(153,Type.Id,new String[]{"S","36"});  
      insert(153,Type.Bool,new String[]{"S","35"});  
      insert(153,Type.OPneg,new String[]{"S","44"});  
      insert(153,Type.LOGOPnot,new String[]{"S","154"});  
      insert(153,Type.LEFTP,new String[]{"S","153"});  
      
      insert(154,Type.Char,new String[]{"S","33"});  
      insert(154,Type.Num,new String[]{"S","32"});  
      insert(154,Type.Literal,new String[]{"S","34"});  
      insert(154,Type.Id,new String[]{"S","36"});  
      insert(154,Type.Bool,new String[]{"S","35"});  
      insert(154,Type.OPneg,new String[]{"S","44"});  
      insert(154,Type.LOGOPnot,new String[]{"S","154"});  
      insert(154,Type.LEFTP,new String[]{"S","153"});  
      
      insert(155,Type.LOGOPand,new String[]{"R",Variable.Condition.toString(),Variable.Relational.toString()});  
      insert(155,Type.LOGOPor,new String[]{"R",Variable.Condition.toString(),Variable.Relational.toString()});  
      insert(155,Type.LOGOPnand,new String[]{"R",Variable.Condition.toString(),Variable.Relational.toString()});  
      insert(155,Type.LOGOPnor,new String[]{"R",Variable.Condition.toString(),Variable.Relational.toString()});  
      insert(155,Type.RIGHTP,new String[]{"R",Variable.Condition.toString(),Variable.Relational.toString()});  
      insert(155,Type.BY,new String[]{"R",Variable.Condition.toString(),Variable.Relational.toString()});       
      
      insert(156,Type.IF,new String[]{"R",Variable.Loop.toString(),Type.RESURRECT.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataValNum.toString(),Type.UNTIL.toString(),Variable.Condition.toString(),Type.BY.toString(),Variable.dataValNum.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Type.RIGHTCU.toString()});  
      insert(156,Type.RESURRECT,new String[]{"R",Variable.Loop.toString(),Type.RESURRECT.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataValNum.toString(),Type.UNTIL.toString(),Variable.Condition.toString(),Type.BY.toString(),Variable.dataValNum.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Type.RIGHTCU.toString()});  
      insert(156,Type.PRAY,new String[]{"R",Variable.Loop.toString(),Type.RESURRECT.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataValNum.toString(),Type.UNTIL.toString(),Variable.Condition.toString(),Type.BY.toString(),Variable.dataValNum.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Type.RIGHTCU.toString()});  
      insert(156,Type.UNVEIL,new String[]{"R",Variable.Loop.toString(),Type.RESURRECT.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataValNum.toString(),Type.UNTIL.toString(),Variable.Condition.toString(),Type.BY.toString(),Variable.dataValNum.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Type.RIGHTCU.toString()});  
      insert(156,Type.Id,new String[]{"R",Variable.Loop.toString(),Type.RESURRECT.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataValNum.toString(),Type.UNTIL.toString(),Variable.Condition.toString(),Type.BY.toString(),Variable.dataValNum.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Type.RIGHTCU.toString()});  
      insert(156,Type.RIGHTCU,new String[]{"R",Variable.Loop.toString(),Type.RESURRECT.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataValNum.toString(),Type.UNTIL.toString(),Variable.Condition.toString(),Type.BY.toString(),Variable.dataValNum.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Type.RIGHTCU.toString()});  
      insert(156,Type.SUMMON,new String[]{"R",Variable.Loop.toString(),Type.RESURRECT.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataValNum.toString(),Type.UNTIL.toString(),Variable.Condition.toString(),Type.BY.toString(),Variable.dataValNum.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Type.RIGHTCU.toString()});  
     
      insert(157,Type.RELOPgt,new String[]{"S","199"});  
      insert(157,Type.RELOPlt,new String[]{"S","200"});  
      insert(157,Type.RELOPge,new String[]{"S","202"});  
      insert(157,Type.RELOPle,new String[]{"S","203"});  
      insert(157,Type.RELOPeq,new String[]{"S","201"});  
      insert(157,Type.RELOPne,new String[]{"S","204"});  
      insert(157, Type.LOGOPand, new String[]{"R", Variable.Condition.toString(), Variable.dataVal.toString()});
      insert(157, Type.LOGOPnand, new String[]{"R", Variable.Condition.toString(), Variable.dataVal.toString()});
      insert(157, Type.LOGOPor, new String[]{"R", Variable.Condition.toString(), Variable.dataVal.toString()});
      insert(157, Type.LOGOPnor, new String[]{"R", Variable.Condition.toString(), Variable.dataVal.toString()});
      insert(157, Type.RIGHTP, new String[]{"R", Variable.Condition.toString(), Variable.dataVal.toString()});
      //missing state 158, 159 cant find 
      
      insert(160,Type.IF,new String[]{"R",Variable.IfStatement.toString(),Type.IF.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(160,Type.ELSE,new String[]{"S","161"});  
      insert(160,Type.RESURRECT,new String[]{"R",Variable.IfStatement.toString(),Type.IF.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(160,Type.PRAY,new String[]{"R",Variable.IfStatement.toString(),Type.IF.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(160,Type.UNVEIL,new String[]{"R",Variable.IfStatement.toString(),Type.IF.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(160,Type.Id,new String[]{"R",Variable.IfStatement.toString(),Type.IF.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(160,Type.SUMMON,new String[]{"R",Variable.IfStatement.toString(),Type.IF.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(160,Type.RIGHTCU,new String[]{"R",Variable.IfStatement.toString(),Type.IF.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      
      
      insert(161,Type.LEFTCU,new String[]{"S","162"});  
      
      insert(162,Type.IF,new String[]{"S","99"});  
      insert(162,Type.RESURRECT,new String[]{"S","137"});  
      insert(162,Type.PRAY,new String[]{"S","78"});  
      insert(162,Type.UNVEIL,new String[]{"S","88"});  
      insert(162,Type.Id,new String[]{"S","98"});  
      insert(162,Type.SUMMON,new String[]{"S","134"});  
      
      insert(163,Type.RIGHTCU,new String[]{"S","164"});  
      
      insert(164,Type.IF,new String[]{"R",Variable.IfStatement.toString(),Type.IF.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString(),Type.ELSE.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(164,Type.RESURRECT,new String[]{"R",Variable.IfStatement.toString(),Type.IF.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString(),Type.ELSE.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(164,Type.PRAY,new String[]{"R",Variable.IfStatement.toString(),Type.IF.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString(),Type.ELSE.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(164,Type.UNVEIL,new String[]{"R",Variable.IfStatement.toString(),Type.IF.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString(),Type.ELSE.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(164,Type.Id,new String[]{"R",Variable.IfStatement.toString(),Type.IF.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString(),Type.ELSE.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(164,Type.SUMMON,new String[]{"R",Variable.IfStatement.toString(),Type.IF.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString(),Type.ELSE.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(164,Type.RIGHTCU,new String[]{"R",Variable.IfStatement.toString(),Type.IF.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString(),Type.ELSE.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
     
      
      insert(165,Type.Char,new String[]{"S","33"});
      insert(165,Type.Num,new String[]{"S","32"});  
      insert(165,Type.Literal,new String[]{"S","34"});  
      insert(165,Type.Id,new String[]{"S","36"});  
      insert(165,Type.Bool,new String[]{"S","35"});  
      insert(165,Type.OPneg,new String[]{"S","44"});  
      insert(165,Type.LOGOPnot,new String[]{"S","154"});  
      insert(165,Type.LEFTP,new String[]{"S","153"});  
      
      insert(166, Type.LOGOPnot, new String []{"R", Variable.Logic.toString(), Type.LOGOPand.toString()});
      insert(166, Type.LEFTP, new String []{"R", Variable.Logic.toString(), Type.LOGOPand.toString()});
      insert(166, Type.Num, new String []{"R", Variable.Logic.toString(), Type.LOGOPand.toString()});
      insert(166, Type.Char, new String []{"R", Variable.Logic.toString(), Type.LOGOPand.toString()});
      insert(166, Type.Literal, new String []{"R", Variable.Logic.toString(), Type.LOGOPand.toString()});
      insert(166, Type.Bool, new String []{"R", Variable.Logic.toString(), Type.LOGOPand.toString()});
      insert(166, Type.OPneg, new String []{"R", Variable.Logic.toString(), Type.LOGOPand.toString()});
      insert(166, Type.Id, new String []{"R", Variable.Logic.toString(), Type.LOGOPand.toString()});
      
      insert(167, Type.LOGOPnot, new String []{"R", Variable.Logic.toString(), Type.LOGOPor.toString()});
      insert(167, Type.LEFTP, new String []{"R", Variable.Logic.toString(), Type.LOGOPor.toString()});
      insert(167, Type.Num, new String []{"R", Variable.Logic.toString(), Type.LOGOPor.toString()});
      insert(167, Type.Char, new String []{"R", Variable.Logic.toString(), Type.LOGOPor.toString()});
      insert(167, Type.Literal, new String []{"R", Variable.Logic.toString(), Type.LOGOPor.toString()});
      insert(167, Type.Bool, new String []{"R", Variable.Logic.toString(), Type.LOGOPor.toString()});
      insert(167, Type.OPneg, new String []{"R", Variable.Logic.toString(), Type.LOGOPor.toString()});
      insert(167, Type.Id, new String []{"R", Variable.Logic.toString(), Type.LOGOPor.toString()});
      
      insert(168, Type.LOGOPnot, new String []{"R", Variable.Logic.toString(), Type.LOGOPnand.toString()});
      insert(168, Type.LEFTP, new String []{"R", Variable.Logic.toString(), Type.LOGOPnand.toString()});
      insert(168, Type.Num, new String []{"R", Variable.Logic.toString(), Type.LOGOPnand.toString()});
      insert(168, Type.Char, new String []{"R", Variable.Logic.toString(), Type.LOGOPnand.toString()});
      insert(168, Type.Literal, new String []{"R", Variable.Logic.toString(), Type.LOGOPnand.toString()});
      insert(168, Type.Bool, new String []{"R", Variable.Logic.toString(), Type.LOGOPnand.toString()});
      insert(168, Type.OPneg, new String []{"R", Variable.Logic.toString(), Type.LOGOPnand.toString()});
      insert(168, Type.Id, new String []{"R", Variable.Logic.toString(), Type.LOGOPnand.toString()});
      
      insert(169, Type.LOGOPnot, new String []{"R", Variable.Logic.toString(), Type.LOGOPnor.toString()});
      insert(169, Type.LEFTP, new String []{"R", Variable.Logic.toString(), Type.LOGOPnor.toString()});
      insert(169, Type.Num, new String []{"R", Variable.Logic.toString(), Type.LOGOPnor.toString()});
      insert(169, Type.Char, new String []{"R", Variable.Logic.toString(), Type.LOGOPnor.toString()});
      insert(169, Type.Literal, new String []{"R", Variable.Logic.toString(), Type.LOGOPnor.toString()});
      insert(169, Type.Bool, new String []{"R", Variable.Logic.toString(), Type.LOGOPnor.toString()});
      insert(169, Type.OPneg, new String []{"R", Variable.Logic.toString(), Type.LOGOPnor.toString()});
      insert(169, Type.Id, new String []{"R", Variable.Logic.toString(), Type.LOGOPnor.toString()});
      
      insert(170,Type.LOGOPand,new String[]{"S","166"});  
      insert(170,Type.LOGOPor,new String[]{"S","167"});  
      insert(170,Type.LOGOPnand,new String[]{"S","168"});  
      insert(170,Type.LOGOPnor,new String[]{"S","169"});  
      insert(170,Type.RIGHTP,new String[]{"S","171"});  
      
      insert(171,Type.LOGOPand,new String[]{"R",Variable.Condition.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString()});  
      insert(171,Type.LOGOPor,new String[]{"R",Variable.Condition.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString()});  
      insert(171,Type.LOGOPnand,new String[]{"R",Variable.Condition.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString()});  
      insert(171,Type.LOGOPnor,new String[]{"R",Variable.Condition.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString()});  
      insert(171,Type.RIGHTP,new String[]{"R",Variable.Condition.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString()}); 
      insert(171,Type.BY,new String[]{"R",Variable.Condition.toString(),Type.LEFTP.toString(),Variable.Condition.toString(),Type.RIGHTP.toString()});  
      
      insert(172,Type.LOGOPand,new String[]{"R",Variable.Condition.toString(),Type.LOGOPnot.toString(),Variable.Condition.toString()});  
      insert(172,Type.LOGOPor,new String[]{"R",Variable.Condition.toString(),Type.LOGOPnot.toString(),Variable.Condition.toString()});  
      insert(172,Type.LOGOPnand,new String[]{"R",Variable.Condition.toString(),Type.LOGOPnot.toString(),Variable.Condition.toString()});  
      insert(172,Type.LOGOPnor,new String[]{"R",Variable.Condition.toString(),Type.LOGOPnot.toString(),Variable.Condition.toString()});  
      insert(172,Type.RIGHTP,new String[]{"R",Variable.Condition.toString(),Type.LOGOPnot.toString(),Variable.Condition.toString()}); 
      insert(172,Type.BY,new String[]{"R",Variable.Condition.toString(),Type.LOGOPnot.toString(),Variable.Condition.toString()}); 
      
      insert(173,Type.Char,new String[]{"S","33"});  
      insert(173,Type.Num,new String[]{"S","32"});  
      insert(173,Type.Literal,new String[]{"S","34"});  
      insert(173,Type.Id,new String[]{"S","36"});  
      insert(173,Type.Bool,new String[]{"S","35"});  
      insert(173,Type.OPneg,new String[]{"S","44"});  
      
      insert(174,Type.LOGOPand,new String[]{"R",Variable.Relational.toString(),Variable.dataVal.toString(),Variable.CondiSign.toString(),Variable.dataVal.toString()});  
      insert(174,Type.LOGOPor,new String[]{"R",Variable.Relational.toString(),Variable.dataVal.toString(),Variable.CondiSign.toString(),Variable.dataVal.toString()});  
      insert(174,Type.LOGOPnand,new String[]{"R",Variable.Relational.toString(),Variable.dataVal.toString(),Variable.CondiSign.toString(),Variable.dataVal.toString()});  
      insert(174,Type.LOGOPnor,new String[]{"R",Variable.Relational.toString(),Variable.dataVal.toString(),Variable.CondiSign.toString(),Variable.dataVal.toString()});  
      insert(174,Type.RIGHTP,new String[]{"R",Variable.Relational.toString(),Variable.dataVal.toString(),Variable.CondiSign.toString(),Variable.dataVal.toString()});  
      insert(174,Type.BY,new String[]{"R",Variable.Relational.toString(),Variable.dataVal.toString(),Variable.CondiSign.toString(),Variable.dataVal.toString()});  
      
      
      insert(175,Type.Char,new String[]{"S","33"});  
      insert(175,Type.Num,new String[]{"S","142"});  
      insert(175,Type.Literal,new String[]{"S","34"});  
      insert(175,Type.Id,new String[]{"S","141"});  
      insert(175,Type.Bool,new String[]{"S","35"});  
      insert(175,Type.OPneg,new String[]{"S","143"});    
      insert(175,Type.LEFTP,new String[]{"S","179"});  
      
      insert(176,Type.eos,new String[]{"S","177"});  
      
      insert(177,Type.IF,new String[]{"R",Variable.Declare.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataVal.toString(),Type.eos.toString()});  
      insert(177,Type.RESURRECT,new String[]{"R",Variable.Declare.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataVal.toString(),Type.eos.toString()});  
      insert(177,Type.PRAY,new String[]{"R",Variable.Declare.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataVal.toString(),Type.eos.toString()});  
      insert(177,Type.UNVEIL,new String[]{"R",Variable.Declare.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataVal.toString(),Type.eos.toString()});  
      insert(177,Type.Id,new String[]{"R",Variable.Declare.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataVal.toString(),Type.eos.toString()});  
      insert(177,Type.RIGHTCU,new String[]{"R",Variable.Declare.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataVal.toString(),Type.eos.toString()});  
      insert(177,Type.SUMMON,new String[]{"R",Variable.Declare.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataVal.toString(),Type.eos.toString()});  
      
      insert(178,Type.eos,new String[]{"S","195"});  
      insert(178,Type.OPplus,new String[]{"S","180"});  
      insert(178,Type.OPminus,new String[]{"S","181"});  
      insert(178,Type.OPtimes,new String[]{"S","182"});  
      
      insert(179,Type.Char,new String[]{"S","33"});  
      insert(179,Type.Num,new String[]{"S","142"});  
      insert(179,Type.Literal,new String[]{"S","34"});  
      insert(179,Type.Id,new String[]{"S","141"});  
      insert(179,Type.Bool,new String[]{"S","35"});  
      insert(179,Type.OPneg,new String[]{"S","143"});    
      insert(179,Type.LEFTP,new String[]{"S","179"});  
      
      insert(180,Type.LEFTP,new String[]{"R",Variable.MathSign.toString(),Type.OPplus.toString()});
      insert(180,Type.Id,new String[]{"R",Variable.MathSign.toString(),Type.OPplus.toString()});
      insert(180,Type.Num,new String[]{"R",Variable.MathSign.toString(),Type.OPplus.toString()});
      insert(180,Type.OPneg,new String[]{"R",Variable.MathSign.toString(),Type.OPplus.toString()});
      
      insert(181,Type.LEFTP,new String[]{"R",Variable.MathSign.toString(),Type.OPminus.toString()});
      insert(181,Type.Id,new String[]{"R",Variable.MathSign.toString(),Type.OPminus.toString()});
      insert(181,Type.Num,new String[]{"R",Variable.MathSign.toString(),Type.OPminus.toString()});
      insert(181,Type.OPneg,new String[]{"R",Variable.MathSign.toString(),Type.OPminus.toString()});
      
      insert(182,Type.LEFTP,new String[]{"R",Variable.MathSign.toString(),Type.OPtimes.toString()});
      insert(182,Type.Id,new String[]{"R",Variable.MathSign.toString(),Type.OPtimes.toString()});
      insert(182,Type.Num,new String[]{"R",Variable.MathSign.toString(),Type.OPtimes.toString()});
      insert(182,Type.OPneg,new String[]{"R",Variable.MathSign.toString(),Type.OPtimes.toString()});
      
      
      insert(183,Type.UNTIL,new String[]{"S","184"});  
      
      insert(184,Type.Char,new String[]{"S","33"});  
      insert(184,Type.Num,new String[]{"S","32"});  
      insert(184,Type.Literal,new String[]{"S","34"});  
      insert(184,Type.Id,new String[]{"S","36"});  
      insert(184,Type.Bool,new String[]{"S","35"});  
      insert(184,Type.OPneg,new String[]{"S","44"});  
      insert(184,Type.LOGOPnot,new String[]{"S","154"});   
      insert(184,Type.LEFTP,new String[]{"S","153"});   
      
      insert(185,Type.LOGOPand,new String[]{"S","166"});   
      insert(185,Type.LOGOPor,new String[]{"S","167"});   
      insert(185,Type.LOGOPnand,new String[]{"S","168"});   
      insert(185,Type.LOGOPnor,new String[]{"S","169"});   
      insert(185,Type.BY,new String[]{"S","186"});   
      
      insert(186,Type.Num,new String[]{"S","142"});   
      insert(186,Type.Id,new String[]{"S","141"});  
      insert(186,Type.OPneg,new String[]{"S","143"}); 
      
      insert(187,Type.RIGHTP,new String[]{"S","188"}); 
      
      insert(188,Type.LEFTCU,new String[]{"S","189"});  
      
      insert(189,Type.RESURRECT,new String[]{"S","137"});  
      insert(189,Type.PRAY,new String[]{"S","78"});  
      insert(189,Type.Id,new String[]{"S","98"});  
      insert(189,Type.SUMMON,new String[]{"S","134"});  
      insert(189,Type.IF,new String[]{"S","99"});  
      insert(189,Type.UNVEIL,new String[]{"S","88"}); 
      insert(189,Type.RIGHTCU,new String[]{"S","156"}); 
      
      insert(190,Type.RIGHTCU,new String[]{"S","191"});  
      
      insert(191,Type.IF,new String[]{"R",Variable.Loop.toString(),Type.RESURRECT.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataValNum.toString(),Type.UNTIL.toString(),Variable.Condition.toString(),Type.BY.toString(),Variable.dataValNum.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(191,Type.RESURRECT,new String[]{"R",Variable.Loop.toString(),Type.RESURRECT.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataValNum.toString(),Type.UNTIL.toString(),Variable.Condition.toString(),Type.BY.toString(),Variable.dataValNum.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(191,Type.PRAY,new String[]{"R",Variable.Loop.toString(),Type.RESURRECT.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataValNum.toString(),Type.UNTIL.toString(),Variable.Condition.toString(),Type.BY.toString(),Variable.dataValNum.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(191,Type.UNVEIL,new String[]{"R",Variable.Loop.toString(),Type.RESURRECT.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataValNum.toString(),Type.UNTIL.toString(),Variable.Condition.toString(),Type.BY.toString(),Variable.dataValNum.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(191,Type.Id,new String[]{"R",Variable.Loop.toString(),Type.RESURRECT.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataValNum.toString(),Type.UNTIL.toString(),Variable.Condition.toString(),Type.BY.toString(),Variable.dataValNum.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(191,Type.RIGHTCU,new String[]{"R",Variable.Loop.toString(),Type.RESURRECT.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataValNum.toString(),Type.UNTIL.toString(),Variable.Condition.toString(),Type.BY.toString(),Variable.dataValNum.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      insert(191,Type.SUMMON,new String[]{"R",Variable.Loop.toString(),Type.RESURRECT.toString(),Type.LEFTP.toString(),Type.Id.toString(),Type.IS.toString(),Variable.dataValNum.toString(),Type.UNTIL.toString(),Variable.Condition.toString(),Type.BY.toString(),Variable.dataValNum.toString(),Type.RIGHTP.toString(),Type.LEFTCU.toString(),Variable.Main.toString(),Type.RIGHTCU.toString()});  
      
      insert(192,Type.IF,new String[]{"S","99"});  
      insert(192,Type.RESURRECT,new String[]{"S","137"});  
      insert(192,Type.PRAY,new String[]{"S","78"});  
      insert(192,Type.UNVEIL,new String[]{"S","88"});  
      insert(192,Type.Id,new String[]{"S","98"});  
      insert(192,Type.SUMMON,new String[]{"S","134"});  
      insert(192,Type.RIGHTCU,new String[]{"R",Variable.code.toString(),Type.BAPTIZE.toString(),Type.LEFTSQ.toString(),Variable.vList.toString(),Type.RIGHTSQ.toString(),Type.eos.toString()});  

      
      
      insert(193,Type.OPplus,new String[]{"S","180"});  
      insert(193,Type.OPminus,new String[]{"S","181"});  
      insert(193,Type.OPtimes,new String[]{"S","182"});  
      insert(193,Type.RIGHTP,new String[]{"S","194"});  
      
      insert(194,Type.eos,new String[]{"R",Variable.Mathematical.toString(),Type.LEFTP.toString(),Variable.Mathematical.toString(),Type.RIGHTP.toString()});  
      insert(194,Type.RIGHTP,new String[]{"R",Variable.Mathematical.toString(),Type.LEFTP.toString(),Variable.Mathematical.toString(),Type.RIGHTP.toString()});  
      insert(194,Type.OPplus,new String[]{"R",Variable.Mathematical.toString(),Type.LEFTP.toString(),Variable.Mathematical.toString(),Type.RIGHTP.toString()});  
      insert(194,Type.OPminus,new String[]{"R",Variable.Mathematical.toString(),Type.LEFTP.toString(),Variable.Mathematical.toString(),Type.RIGHTP.toString()});  
      insert(194,Type.OPtimes,new String[]{"R",Variable.Mathematical.toString(),Type.LEFTP.toString(),Variable.Mathematical.toString(),Type.RIGHTP.toString()});  
   
      insert(195, Type.Id, new String[]{"R", Variable.Declare.toString(), Type.Id.toString(), Type.IS.toString(), Variable.Mathematical.toString(), Type.eos.toString()});
      insert(195, Type.SUMMON, new String[]{"R", Variable.Declare.toString(), Type.Id.toString(), Type.IS.toString(), Variable.Mathematical.toString(), Type.eos.toString()});
      insert(195, Type.IF, new String[]{"R", Variable.Declare.toString(), Type.Id.toString(), Type.IS.toString(), Variable.Mathematical.toString(), Type.eos.toString()});
      insert(195, Type.RESURRECT, new String[]{"R", Variable.Declare.toString(), Type.Id.toString(), Type.IS.toString(), Variable.Mathematical.toString(), Type.eos.toString()});
      insert(195, Type.RIGHTCU, new String[]{"R", Variable.Declare.toString(), Type.Id.toString(), Type.IS.toString(), Variable.Mathematical.toString(), Type.eos.toString()});
      insert(195, Type.UNVEIL, new String[]{"R", Variable.Declare.toString(), Type.Id.toString(), Type.IS.toString(), Variable.Mathematical.toString(), Type.eos.toString()});
      insert(195, Type.PRAY, new String[]{"R", Variable.Declare.toString(), Type.Id.toString(), Type.IS.toString(), Variable.Mathematical.toString(), Type.eos.toString()});
      
      insert(196,Type.OPplus,new String[]{"S","180"});  
      insert(196,Type.OPminus,new String[]{"S","181"});  
      insert(196,Type.OPtimes,new String[]{"S","182"});  
      insert(196,Type.eos,new String[]{"R",Variable.Mathematical.toString(),Variable.Mathematical.toString(),Variable.MathSign.toString(),Variable.Mathematical.toString()});  
      insert(196,Type.RIGHTP,new String[]{"R",Variable.Mathematical.toString(),Variable.Mathematical.toString(),Variable.MathSign.toString(),Variable.Mathematical.toString()});  
      insert(196,Type.BY,new String[]{"R",Variable.Condition.toString(),Variable.Condition.toString(),Variable.Logic.toString(),Variable.Condition.toString()});  
       //^not sure with reduce by BY
     
      insert(197,Type.eos,new String[]{"R",Variable.Mathematical.toString(),Variable.dataValNum.toString()});  
      insert(197,Type.OPplus,new String[]{"R",Variable.Mathematical.toString(),Variable.dataValNum.toString()});  
      insert(197,Type.OPminus,new String[]{"R",Variable.Mathematical.toString(),Variable.dataValNum.toString()});  
      insert(197,Type.OPtimes,new String[]{"R",Variable.Mathematical.toString(),Variable.dataValNum.toString()});  
      insert(197,Type.RIGHTP,new String[]{"R",Variable.Mathematical.toString(),Variable.dataValNum.toString()});  
      
      insert(198,Type.LOGOPand,new String[]{"S","166"});  
      insert(198,Type.LOGOPor,new String[]{"S","167"});  
      insert(198,Type.LOGOPnand,new String[]{"S","168"});  
      insert(198,Type.LOGOPnor,new String[]{"S","169"});  
      insert(198,Type.RIGHTP,new String[]{"R",Variable.Condition.toString(),Variable.Condition.toString(),Variable.Logic.toString(),Variable.Condition.toString()});  
      insert(198,Type.BY,new String[]{"R",Variable.Condition.toString(),Variable.Condition.toString(),Variable.Logic.toString(),Variable.Condition.toString()});  //questionable lookahead
         
      insert(199,Type.Num,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPgt.toString()});
      insert(199,Type.Char,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPgt.toString()});   
      insert(199,Type.Literal,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPgt.toString()});   
      insert(199,Type.Bool,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPgt.toString()});   
      insert(199,Type.OPneg,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPgt.toString()});   
      insert(199,Type.Id,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPgt.toString()});   
      
      insert(200,Type.Num,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPlt.toString()});
      insert(200,Type.Char,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPlt.toString()});   
      insert(200,Type.Literal,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPlt.toString()});   
      insert(200,Type.Bool,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPlt.toString()});   
      insert(200,Type.OPneg,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPlt.toString()});   
      insert(200,Type.Id,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPlt.toString()});   
        
      insert(201,Type.Num,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPeq.toString()});
      insert(201,Type.Char,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPeq.toString()});   
      insert(201,Type.Literal,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPeq.toString()});   
      insert(201,Type.Bool,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPeq.toString()});   
      insert(201,Type.OPneg,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPeq.toString()});   
      insert(201,Type.Id,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPeq.toString()});   
          
      insert(202,Type.Num,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPge.toString()});
      insert(202,Type.Char,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPge.toString()});   
      insert(202,Type.Literal,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPge.toString()});   
      insert(202,Type.Bool,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPge.toString()});   
      insert(202,Type.OPneg,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPge.toString()});   
      insert(202,Type.Id,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPge.toString()});   
          
      insert(203,Type.Num,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPle.toString()});
      insert(203,Type.Char,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPle.toString()});   
      insert(203,Type.Literal,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPle.toString()});   
      insert(203,Type.Bool,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPle.toString()});   
      insert(203,Type.OPneg,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPle.toString()});   
      insert(203,Type.Id,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPle.toString()});   
          
      insert(204,Type.Num,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPne.toString()});
      insert(204,Type.Char,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPne.toString()});   
      insert(204,Type.Literal,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPne.toString()});   
      insert(204,Type.Bool,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPne.toString()});   
      insert(204,Type.OPneg,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPne.toString()});   
      insert(204,Type.Id,new String[]{"R",Variable.CondiSign.toString(),Type.RELOPne.toString()});   
          
      insert(205,Type.OPneg,new String[]{"S","143"});  
      insert(205,Type.LEFTP,new String[]{"S","179"});  
      insert(205,Type.Num,new String[]{"S","142"}); 
      insert(205, Type.Id, new String[]{"S","141"});
 
    }
    
    
    private void insert(int state, Type tok, String[] value){
        if(row[state]==null){
            row[state] = new HashMap<Type, String[]>();
        }
        row[state].put(tok, value);
    }
    
    public String[] get(int state, Type tok){
        return row[state].get(tok);
    }
    
   
}