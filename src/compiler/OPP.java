
package compiler;

import java.util.Stack;
public class OPP{
    private int top = 0;
    private final static int stackMax = 100;
    private Object[] stk = new Object[stackMax+1];//not using index 0. So from 1-stackMax indeces //cant instantiate a generic array
    
    public void clear()
    {
        top = 0;
    }
    
    public boolean isEmpty()
    {
        return top == 0;
    }
    
    public static String infixToPrefix(String e, boolean is_logical)
    {
        if(e.length()==0)//if empty string
            return "";
        String prefix="";
        String arr[] = e.split(" ");
        Stack<Object> operator = new Stack<>();
        Stack<Object> operand = new Stack<>();
        if (!is_logical) {
            for(int x=arr.length-1; x>=0; x--)
            {
                if(!(arr[x].charAt(0)=='(' || arr[x].charAt(0)==')' || arr[x].charAt(0)=='/' || arr[x].charAt(0)=='+' || arr[x].charAt(0)=='-' || arr[x].charAt(0)=='*'|| arr[x].charAt(0)=='^' || arr[x].charAt(0)=='(' || arr[x].charAt(0)==')'))
                    operand.push(arr[x]); //puts all operand to stack
                else //all non operand
                {

                    if(arr[x].charAt(0)=='(') //if see an open parenthesis. pop til find closing partner
                        //push all popped to operand stack
                    {
                        while(operator.peek().toString().compareTo(")")!=0)
                            operand.push(operator.pop());
                        operator.pop(); //pop the closing but not push to operand
                    }
                    else
                    {   //since this is RIGHT to LEFT notation, the incoming '+' must be greater than instack '+'
                        //so incoming should have greater precedence if same operation
                        //sinks the incoming operator downward till it is not lowest. highest top. lowest bottom.                
                        while((!operator.isEmpty()) && (incomingRL(arr[x].charAt(0)) < instackRL(operator.peek().toString().charAt(0))))
                            operand.push(operator.pop());
                        operator.push(arr[x]); //push the incoming to stack
                    }
                }
            }
        } else {
            for(int x=arr.length-1; x>=0; x--)
            {
                if(!(arr[x].substring(0).equals("(") || arr[x].substring(0).equals(")") 
                        || arr[x].substring(0).equals("NOT") || arr[x].substring(0).equals("NOR")
                        || arr[x].substring(0).equals("NAND") || arr[x].substring(0).equals("OR") 
                        || arr[x].substring(0).equals("AND")|| arr[x].substring(0).equals(">=")
                        || arr[x].substring(0).equals("!=")
                        || arr[x].substring(0).equals("<=") 
                        || arr[x].substring(0).equals("<") 
                        || arr[x].substring(0).equals(">") 
                        || arr[x].substring(0).equals("=")))
                    operand.push(arr[x]); //puts all operand to stack
                else //all non operand
                {

                    if(arr[x].charAt(0)=='(') //if see an open parenthesis. pop til find closing partner
                        //push all popped to operand stack
                    {
                        while(operator.peek().toString().compareTo(")")!=0)
                            operand.push(operator.pop());
                        operator.pop(); //pop the closing but not push to operand
                    }
                    else
                    {   //since this is RIGHT to LEFT notation, the incoming '+' must be greater than instack '+'
                        //so incoming should have greater precedence if same operation
                        //sinks the incoming operator downward till it is not lowest. highest top. lowest bottom.                
                        while((!operator.isEmpty()) && (LOGincomingRL(arr[x].substring(0)) < LOGinstackRL(operator.peek().toString().substring(0))))
                            operand.push(operator.pop());
                        operator.push(arr[x]); //push the incoming to stack
                    }
                }
            }
        }
        
        while(!operator.isEmpty())
            operand.push(operator.pop());
        while(!operand.isEmpty())
            prefix = prefix + " " + operand.pop().toString();
        prefix = prefix.substring(1, prefix.length());
        return prefix;
    }
    
    private static int LOGincomingRL(String operator)
    {
        switch(operator)
        {   
            case "NOR": 
            case "OR": return 12;
            case "NAND":
            case "AND": return 14;
            case "NOT": return 16;
            case "=":
            case "<":
            case "<=":
            case ">":
            case "!=":
            case ">=": return 24;
            case ")": 
            case "(": return 29;
            default : return -1;
        }
    }
    
    private static int LOGinstackRL(String operator)
    {
        switch(operator)
        {   
            case "NOR":
            case "OR": return 11;
            case "NAND":
            case "AND": return 13;
            case "NOT": return 15;
            case "=":
            case "<":
            case "<=":
            case ">":
            case "!=":
            case ">=": return 23;
            case ")":
            case "(": return 0;
            default : return -1;
            
        }
    }
    
    private static int incomingRL(char operator)
    {
        switch(operator)
        {
            case '+':
            case '-': return 2;
            case '*': return 4;
            case ')': 
            case '(': return 9;
            default : return -1;
        }
    }
    
    private static int instackRL(char operator)
    {
        switch(operator)
        {
            case '+':
            case '-': return 1;
            case '*': return 3;
            case ')':
            case '(': return 0;
            default : return -1;
            
        }
    }
    
    public static boolean isOperand(String e) {
        switch(e) {
           case "NEG":
           case "NOR":
           case "OR": 
           case "NAND":
           case "AND": 
           case "NOT":
           case "=":
           case "<":
           case "<=":
           case ">":
           case "!=":
           case ">=":
           case "*": 
           case "+": 
           case "-": 
           case "(": 
           case ")": return false;
        }
        return true;
    }
    
    public static Tree prefixToTree(String e){
        //implement later
        String es[] = e.split(" ");
        Stack<Tree> forest = new Stack<>();
        for(int i=es.length-1;i>=0;i--){
            if(isOperand(es[i])){
                String operandType = Semantics.checkValueType(es[i]);
                Tree temp = new Tree(operandType,es[i]);
                forest.push(temp);
            } else {
                if(es[i].compareTo("NEG") == 0 || es[i].compareTo("NOT") == 0){
                    Stack<Tree> zoo = new Stack<>();
                    zoo.push(forest.pop());
                    String operatorType;
                    if(es[i].compareTo("NEG") == 0)
                        operatorType = Type.OPneg.toString();
                    else
                        operatorType = Type.LOGOPnot.toString();
                    Tree temp = new Tree(-2, operatorType, zoo);
                    temp.root.lexeme = es[i];
                    forest.push(temp);
                }
                else{
                    Stack<Tree> zoo = new Stack<>();
                    zoo.push(forest.pop());
                    zoo.push(forest.pop());
                    Stack<Tree> zooreverse = new Stack<>();
                    zooreverse.push(zoo.pop());
                    zooreverse.push(zoo.pop());
                    Tree temp = new Tree(-2,es[i],zooreverse);
                    temp.root.lexeme = es[i];
                    forest.push(temp);
                }
            }
        }
        Tree t = forest.pop();
//        String tr = t.toString();
//        System.out.println("##########################");
//        System.out.println("");
//        System.out.println("<paused>");
//        System.out.println(tr);
//        System.out.println("");
//        System.out.println("##########################");
        //Tree exptree = new Tree(null );
        return t;
    }
    
    
}
