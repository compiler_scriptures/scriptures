/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

/**
 *
 * @author Josh
 */
public class Semantics {
    public static Tree tableAndtreeHijack(Symboltable table, Tree tempT) throws SemanticException{
       switch(tempT.root.SymbolName){
           case "Declare" : return getExprTree(tempT); 
           case "Loop":  return getLogTree(tempT, 6);
           case "IfStatement": return getLogTree(tempT, 2);
           case "SecondDiv" : insertMethods(table, tempT); break;
           case "ThirdDiv" : insertVars(table, tempT); break;
           case "code" : insertVars(table, tempT); break;
//           case "Main" : idAndtypeCheck(table, tempT); break;
           case "FirstDiv" : insertProgramName(table, tempT); break;
               //need OPP for Conditional which has parent Loop and IfStatement
       }
       return tempT;
   }
    
    public static void insertMethods(Symboltable table, Tree secondDiv) throws SemanticException{
       Tree mList = secondDiv.root.Children.get(2);
       if(mList.root.SymbolName.compareTo(Variable.mList.toString()) == 0){
           String terminal = mList.getTerminal();
           String terminals[] = terminal.split(" ");
           for(String t : terminals){
               if(t.compareTo(",") != 0){
                   Identifier temp = new Identifier(new Token(Type.Id, t));
                   temp.datatype = 4;
                   temp.isFinal = 0;
                   temp.value = null;
                   temp.list = new HashMap<>();
                   table.insert(temp);
               }
           }
           
       }
       else{
//           System.out.println("mlist not existing");
       }
       //update symbol table
   }
    
    public static void insertVars(Symboltable table, Tree baptize) throws SemanticException{
       //update symbol table
       int dataType = -1;
       int isFinal = -1;

       Tree vList = baptize.root.Children.get(2);
       if(vList.root.SymbolName.compareTo(Variable.vList.toString()) == 0){
           ArrayList<String[]> symbol_term = new ArrayList<>();
           vList.getTermAndSymb(symbol_term);
//           for(int i = 0; i < symbol_term.size(); i++){
//               System.out.println(symbol_term.get(i)[0]);
//               
//               System.out.println(symbol_term.get(i)[1]);
//           }
           
           for(int index = 0;index<symbol_term.size();index++){
               switch(symbol_term.get(index)[1]){
                   case "STRING" : 
                        dataType = 0; 
                        if(index > 0 && symbol_term.get(index - 1)[1].compareTo("CONFIRM") == 0){
                            isFinal = 1;
                        }
                        else{
                            isFinal = 0;
                        }
                        break;
                   case "NUMBER" : 
                        dataType = 2;                       
                        if(index > 0 && symbol_term.get(index - 1)[1].compareTo("CONFIRM") == 0){
                            isFinal = 1;
                        }
                        else{
                            isFinal = 0;
                        }
                        break;
                   case "CHARACTER" : 
                        dataType = 1;
                        if(index > 0 && symbol_term.get(index - 1)[1].compareTo("CONFIRM") == 0){
                            isFinal = 1;
                        }
                        else{
                            isFinal = 0;
                        }
                        break;
                   case "BOOLEAN" : 
                        dataType = 3;
                        if(index > 0 && symbol_term.get(index - 1)[1].compareTo("CONFIRM") == 0){
                            isFinal = 1;
                        }
                        else{
                            isFinal = 0;
                        }
                        break;
               }
                   
               if(symbol_term.get(index)[1].compareTo("Id")==0 && symbol_term.size() > (index - 1) && symbol_term.get(index - 1)[1].compareTo(Type.IS.toString()) != 0){
                   Identifier temp = new Identifier(new Token(Type.Id, symbol_term.get(index)[0])); 
                   temp.datatype = dataType;
                   temp.isFinal = isFinal;
                   if(isFinal == 1){
                       if(index + 2 < symbol_term.size() && symbol_term.get(index + 1)[1].compareTo(Type.IS.toString()) == 0){
                            temp.value = symbol_term.get(index + 2)[0];
                       }
                       else{
                           throw new SemanticException("You did not initialize the constant variable. : " + symbol_term.get(index)[0] );
                       }
                   }
                   typeChecking(temp.value, temp.datatype, temp.name);
                   table.insert(temp);
               }
           }
       }
   }
    public static String checkValueInput(String tempVal, int dataType) throws SemanticException{
        switch(dataType){
            case 0 : 
                tempVal = "\"" + tempVal + "\""; 
                break;
            case 1 : 
                tempVal = String.valueOf(tempVal.charAt(0)); 
                tempVal = tempVal = "\"" + tempVal + "\"";                 
                break;
            case 2 : 
                try{
                    int i = Integer.parseInt(tempVal);
                    tempVal = String.valueOf(i);
                }catch(Exception e){
                    throw new SemanticException("Expected NUMBER found : " + tempVal);
                }
                break;
            case 3 : //semantics
                if(tempVal.compareTo("GOOD") !=0 && tempVal.compareTo("BAD") != 0){
                    throw new SemanticException("Expected BOOLEAN found : " + tempVal);
                }
        }
        return tempVal;
    }
    
    public static void checkUndeclaredIds(Tree [] pts, HashMap[] tables) throws SemanticException{
        HashMap<String, String> loopIds = new HashMap<>();
        for(int i = 1; i < pts.length; i++){
            ArrayList<String[]> symAndTerm = new ArrayList<>();
            pts[i].getTermAndSymb(symAndTerm);
            for(int j = 0; j <symAndTerm.size(); j++){
                String key = symAndTerm.get(j)[0];
//                    System.out.println("current key is :" + key + " : type is " + symAndTerm.get(j)[1]);
                if(j - 2 >= 0 && symAndTerm.get(j)[1].compareTo(Type.Id.toString()) == 0 && symAndTerm.get(j-2)[1].compareTo(Type.RESURRECT.toString()) == 0){
                    loopIds.put(key, key);
                }
                if(symAndTerm.get(j)[1].compareTo(Type.Id.toString()) == 0 && !loopIds.containsKey(key)){
                    if(tables[0].containsKey(key) ||  tables[i].containsKey(key)){
                    }
                    else
                        throw new SemanticException("Undeclared Identifier : " + key);
                }       
            }
        }
    }
    
    public static Tree getExprTree(Tree declareTree){
       Tree mathematical = declareTree.root.Children.get(2);
            
       if(mathematical.root.SymbolName.compareTo(Variable.Mathematical.toString()) == 0){
            String expression = mathematical.getTerminal();
            expression = expression.trim();
            String prefix = OPP.infixToPrefix(expression, false);
//            System.out.println(prefix);
            Tree newTree = OPP.prefixToTree(prefix);

            Stack<Tree> temp = new Stack<>();
            temp.push(newTree);
            ArrayList<Tree> tempList = new ArrayList<>();
            tempList.add(newTree);
            declareTree.root.Children.get(2).root.Children = tempList;
       }
       return declareTree;
   }
    
   public static Tree getLogTree(Tree logTree, int position){
       Tree logical = logTree.root.Children.get(position);
       if(logical.root.SymbolName.compareTo(Variable.Condition.toString()) == 0){
            String logexp = logical.getTerminal();
            logexp = logexp.trim();
            String prefix = OPP.infixToPrefix(logexp, true);
            Tree newTree = OPP.prefixToTree(prefix);
//            System.out.println(logexp + "Logical exp");
//            System.out.println(prefix + "Logical prefix");
//            System.out.println(newTree + "Logical tree");
            Stack<Tree> temp = new Stack<>();
            temp.push(newTree);
            ArrayList<Tree> tempList = new ArrayList<>();
            tempList.add(newTree);
            logTree.root.Children.get(position).root.Children = tempList;
       }
       return logTree;
   }
   
//   public static void idAndtypeCheck(Symboltable table, Tree declareTree){
//       //update symbol table
//       Tree declare = declareTree.root.Children.get(0);
//       if(declare.root.SymbolName.compareTo(Variable.Declare.toString()) == 0){
//           //redundant, refactor
////           System.out.println("#########"+declare.root.Children.get(2).getTerminal(declare.root.Children.get(2)));
////           table.insert(new Identifier(new Token(Type.NUMBER,declare.root.Children.get(0).root.lexeme.toString())));
//           Identifier updating = table.list.get(declare.root.Children.get(0));
////           updating.value = 
//           // how to insert a value in the symbol table
//       }
//   } 

    public static void insertProgramName(Symboltable table, Tree tempT) throws SemanticException {
        if(tempT.root.Children.get(1).root.SymbolName.compareTo(Type.Id.toString()) == 0){
            Identifier id = new Identifier(new Token(Type.Id, tempT.root.Children.get(1).root.lexeme));
            id.datatype = 5;
            id.isFinal = 0;
            id.list = null;
            table.insert(id);
        }
    }
    
    public static void typeChecking(String val, int dtype, String var ) throws SemanticException{
        if(val == null)
            return;
        switch(dtype){
            case 0: if(val.charAt(0) != '\"' && val.charAt(val.lastIndexOf(val)) != '\"'){
                        throw new SemanticException(var + " SHOULD HAVE STRING VALUE FOUND: "+ val);
                    }
                    break;
            case 1: if(val.charAt(0) != '\'' && val.charAt(val.lastIndexOf(val)) != '\''){
                        throw new SemanticException(var + " SHOULD HAVE CHARACTER VALUE FOUND: "+ val);
                    }
                    break;
            case 2: try{
                        int x = Integer.parseInt(val);
                    }catch(Exception e){
                        throw new SemanticException(var + " SHOULD HAVE NUMBER VALUE FOUND: " + val);
                    }
                    break;
            case 3: if (!val.equals("GOOD") && !val.equals("BAD") ){
                       throw new SemanticException(var + " SHOULD HAVE BOOLEAN VALUE FOUND: "+ val);
                    }
                    break;
        }
    }
//
//    public static void insertLoopId(Symboltable table, Tree tempT) throws SemanticException {
//        if(tempT.root.Children.get(2).root.SymbolName.compareTo(Type.Id.toString()) == 0){
//            Identifier id = new Identifier(new Token(Type.Id,tempT.root.Children.get(2).root.lexeme));
//            id.datatype = 2;
//            id.value = null;
//            id.isFinal = 0;
//            id.list = null;
//            table.insert(id);
//        }
//    }
    
    public static String checkValueType(String checking){//checks if string is num, char, literla, good or bad, Id
        String operandType;        
        if(checking.charAt(0) >= 48 && checking.charAt(0) <= 57)    
            operandType = Type.Num.toString();
        else if(checking.charAt(0) == '\"' && checking.charAt(checking.lastIndexOf(checking)) == '\"')
            operandType = Type.Literal.toString();
        else if(checking.charAt(0) == '\'' && checking.charAt(checking.lastIndexOf(checking)) == '\'')
            operandType = Type.Char.toString();
        else if(checking.compareTo(Type.GOOD.toString()) == 0 || checking.compareTo(Type.BAD.toString()) == 0)
            operandType = Type.valueOf(checking).toString();
        else
            operandType = Type.Id.toString();
        return operandType;
    }
    
      public static boolean isNumeric(String str)  
    {  
      try  
      {  
        int d = Integer.parseInt(str);  
      }  
      catch(NumberFormatException nfe)  
      {  
        return false;  
      }  
      return true;  
    }
    
    public static boolean isRelational(String str)  
    {  
      if (str.equals("=")||str.equals("<")||
              str.equals(">")||str.equals("<=")||
              str.equals(">=") ||str.equals("!="))
            return true;  
      else return false;
    }
}
