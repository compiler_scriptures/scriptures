package compiler;

import java.awt.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Stack;

public class Parser {
    LexAnalyzer lex;
    Symboltable table;
    File input;
    Token lookahead;
    boolean consumed = false;
    Stack<Tree> parse;
    Stack<Integer> state;
    ActionTable actionTable;
    GotoTable gotoTable;
    Tree parseTree;
    int reductionNum;
    boolean isAccepted = false;
    boolean afterMain = false;
    Stack<String> incomingScopes;
    Token prevToken;
    Parser(File inputFile, Symboltable global) throws FileNotFoundException, IOException, LEXexception, PARSEexception{
        reductionNum = 0;
        input = inputFile;
        table = global;
        lex = new LexAnalyzer(input, table, true);
        parse = new Stack();
        state = new Stack();
        state.push(0);
        gotoTable = new GotoTable();
        actionTable = new ActionTable();
        incomingScopes = new Stack();
        prevToken = new Token(Type.$, "THIS IS DUMMY");
    }
    
    public void printAllTokens() throws IOException, EOFexception, LEXexception, SemanticException{
        System.out.println("-------------- LEXICAL ANALYZER PHASE ----------------------");
        Symboltable tempTable = new Symboltable();
        LexAnalyzer tempLex = new LexAnalyzer(input, tempTable, false);
        while(!tempLex.isEOF()){
            Token passBack;
            passBack = tempLex.getToken();
            if(passBack.name.toString().compareTo("ERR") == 0){
                throw new LEXexception("Erroneous character sequence : " + passBack.lexeme + " on line : " + tempLex.line);
          
            }
            if(passBack.name.toString().compareTo("WHITESPACE") != 0){
                    if(passBack.name.toString().compareTo("Id") == 0){
                        System.out.print("[ Id = " + passBack.lexeme + " ]");
                    }
                    else if(passBack.name.toString().compareTo("Num") == 0 ||passBack.name.toString().compareTo("Literal") == 0 ||passBack.name.toString().compareTo("Char") == 0 ||passBack.name.toString().compareTo("Bool") == 0 ){
                        System.out.print("[ " + passBack.name.toString() + " = " + passBack.lexeme + " ]");
                    }
                    else
                        System.out.print("[ " + passBack.name + " ]");
            }
        }
        System.out.println("\n");
        System.out.println("------------------ END OF LEXICAL ANALYZER PHASE ----------------------");
    }
    

    
    public void createTree() throws IOException, EOFexception, LEXexception, PARSEexception, SemanticException{
        lookahead = lex.getToken();
        while(!lex.isEOF()){
            
            if(consumed){
                if(lookahead.name.toString().compareTo(Type.WHITESPACE.toString()) != 0)
                    prevToken = lookahead;
                lookahead = lex.getToken();
                consumed = false;
            }
            if(lookahead.name.toString().compareTo("ERR") == 0){
                throw new LEXexception("Erroneous character sequence : " + lookahead.lexeme + " on line : " + lex.line);
            }
            else if(lookahead.name.toString().compareTo("WHITESPACE") == 0){
                consumed = true;
                continue;
            }
            else if(lookahead.name.toString().compareTo(Type.Id.toString()) == 0 && table.list.containsKey(lookahead.lexeme) && table.list.get(lookahead.lexeme).datatype == 4 && prevToken.name.toString().compareTo(Type.RIGHTCU.toString()) == 0){
                System.out.println(prevToken.name);
                incomingScopes.push(lookahead.lexeme); 
                System.out.println("\n\n\n\n");
                System.out.println("setting up incoming scope : " + lookahead.lexeme);
                System.out.println("\n\n\n\n");
            }
             
            
            
            
           
            String[] rule = actionTable.get(state.peek(), lookahead.name);
            if(rule == null){
                System.out.println("Empty Cell on : (" + state.peek() + ", " + lookahead.name.toString() + " )");
                error();
            }
            else{
                switch(rule[0]){
                    case "S" : shift(Integer.parseInt(rule[1])); break;
                    case "R" : reduce(rule); break;
                    default : System.out.print("WTH wrong encoding on "); 
                              System.out.print(lookahead.name.toString());
                              System.out.println(" " + state.peek());
                              error();
                }
            }
            
          
        }
        
        lookahead = new Token(Type.$, "$");
        consumed = false;
        while(!consumed){
            if(isAccept())
                break;
            String[] rule = actionTable.get(state.peek(), lookahead.name);
            if(rule == null){
                System.out.println("Empty Cell on : (" + state.peek() + ", " + lookahead.name.toString() + " )");
                error();
            }
            else{
                switch(rule[0]){
                    case "S" : shift(Integer.parseInt(rule[1])); break;
                    case "R" : reduce(rule); break;
                    default : System.out.print("WTH wrong encoding on "); 
                              System.out.print(lookahead.name.toString());
                              System.out.println(" " + state.peek());
                              error();
                }
            }
        }
        
//        for(Variable key : gotoTable.row[0].keySet()){
//            System.out.println(gotoTable.row[0].get(key));
//        }
//        
//       
//        for(Type t : Type.values()){
//            System.out.println(actionTable.get(0, t));
//        }
        
    }
    
    
    public void shift(int newState){
        System.out.println("Shift on " + lookahead.name + " to " + newState);
        Tree temp = new Tree(lookahead.name.toString(), lookahead.lexeme);
        parse.push(temp);
        state.push(newState);
        consumed = true;
    }
    
    public void reduce(String[] rule) throws PARSEexception, SemanticException{
//        System.out.println(state.peek());
        int i;
        int ctr = rule.length;
        String rhs = "";
        Stack<Tree> children = new Stack();
        for(i = ctr - 1; i >= 2; i--){
            Tree temp = parse.peek();
            if(temp.root.SymbolName.compareTo(rule[i]) == 0){
                children.push(parse.pop());
                state.pop();
                rhs = rule[i] + " " + rhs;
            }
            else{
                System.out.println("Unexpected Reduction Error");
                System.out.println(temp.root.SymbolName + " != " + rule[i]);
                System.out.println(lookahead.name);  
                error();
            }
        }
//        System.out.println(state.peek());
        
        Tree LHS = new Tree(++reductionNum, rule[1], children);
        Variable gotoVar = Variable.valueOf(rule[1]);
//        parse.push(LHS);
        
//        System.out.println(LHS);
        
        Integer gotoNum =  gotoTable.get(state.peek(), gotoVar); //move error
        if(gotoNum == null){
            System.out.println("Illegal goto on ( " + state.peek() + ", " + gotoVar.name() + " )");
            error();
        }
        else
            state.push(gotoNum);
        
        System.out.println("Reduction by " + rule[1] + " -> " + rhs +", goto state " + gotoNum);
        
        //hijack
        
       if(LHS.root.SymbolName.compareTo(Variable.code.toString()) == 0){
            if(!afterMain){
                table.currentScope = "EXODUS";
                System.out.println("\n\n\n\n");
                System.out.println("changed scope to main");
                System.out.println("\n\n\n\n");
                afterMain = true;
            }
            else{
                String scope = incomingScopes.pop();
                table.currentScope = scope;
                System.out.println("\n\n\n\n");
                System.out.println("changed scope to :" + scope);
                System.out.println("\n\n\n\n");
                
            }
        }
        parse.push(Semantics.tableAndtreeHijack(table, LHS));
//        if(LHS.root.SymbolName.compareTo(Variable.Condition.toString()) == 0){
//            System.out.println(LHS);
//        }
//        System.out.println(state.peek());
    }
    
    public boolean isAccept(){
        if(parse.peek().root.SymbolName.compareTo(Variable.SPrime.toString()) == 0 && lookahead.lexeme.compareTo(Type.$.toString()) == 0){
           isAccepted = true;
           parseTree = parse.pop();
            System.out.println("");
            System.out.println("Source Program is Accepted");
            System.out.println("");
//            System.out.println(parseTree);
            return true;
        }
        else {
            return false;
        }
    }
    
    public void error() throws PARSEexception{
        System.out.println("Top token is " + lookahead.name);
        System.out.println("State is " + state.peek());
        if(!parse.isEmpty())
            System.out.println("Top of parse is " + parse.peek().root.SymbolName);
        else
            System.out.println("Parse is empty");
        
        throw new PARSEexception("Parse Error: Input not Accepted on line : " + lex.line);
    }
}
