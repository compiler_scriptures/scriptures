package compiler;

import java.util.HashMap;

/**
 *
 * @author Josh
 */
public class GotoTable {
    public int state = 206;
    public HashMap<Variable,Integer>[] row = new HashMap[state];
    
    GotoTable(){      
        //initialize so all rows have hashmap;
        for(int i = 0; i < state; i++){
            insert(i, Variable.Init, -1);
        }
        //josh here
        insert(0, Variable.SPrime, 0); //for acceptance state
        insert(0,Variable.Start, 1);
        insert(0, Variable.FirstDiv, 2);
        
        insert(2, Variable.SecondDiv, 4);
        
        insert(4, Variable.ThirdDiv, 7);
        
        insert(7, Variable.FourthDiv, 63);
        insert(7, Variable.sBody, 67);
        
        insert(10, Variable.mList, 16);
        
        insert(12, Variable.mList, 13);
        
        insert(19, Variable.vList, 20);
        insert(19, Variable.dataType, 29);
        
        insert(31, Variable.dataVal, 39);
        
        insert(37, Variable.vList, 42);
        insert(37, Variable.dataType, 29);
        insert(37, Variable.vList2, 41);
        
        insert(40, Variable.vList,42);
        insert(40, Variable.dataType, 29);
        insert(40, Variable.vList2, 43);
        
        insert(50, Variable.dataVal, 53);
        
        insert(51, Variable.vList, 42);
        insert(51, Variable.dataType, 29);
        insert(51, Variable.vList2, 52);
        insert(54, Variable.vList, 42);
        insert(54, Variable.dataType, 29);
        insert(54, Variable.vList2, 55);
        insert(56, Variable.dataType, 57);
        insert(59, Variable.dataVal, 60);
        insert(61, Variable.vList, 42);
        insert(61, Variable.dataType, 29);
        insert(61, Variable.vList2, 62);
        insert(63, Variable.FifthDiv, 64);
        insert(67, Variable.Body, 68);
        insert(70, Variable.code, 104);
        
        insert(72, Variable.vList, 75);
        insert(72, Variable.dataType, 29);
        insert(74, Variable.IfStatement, 93);
        insert(74, Variable.MethodCall, 96);
        insert(74, Variable.Declare, 92);
        insert(74, Variable.Print, 95);
        insert(74, Variable.Get, 94);
        insert(74, Variable.Loop, 97);
        insert(74, Variable.Main, 91);
        insert(89, Variable.dataValLiteral, 90);
        insert(92, Variable.IfStatement, 93);
        insert(92, Variable.MethodCall, 96);
        insert(92, Variable.Declare, 92);
        insert(92, Variable.Print, 95);
        insert(92, Variable.Get, 94);
        insert(92, Variable.Loop, 97);
        insert(92, Variable.Main, 100);
        insert(93, Variable.IfStatement, 93);
        insert(93, Variable.MethodCall, 96);
        insert(93, Variable.Declare, 92);
        insert(93, Variable.Print, 95);
        insert(93, Variable.Get, 94);
        insert(93, Variable.Loop, 97);
        insert(93, Variable.Main, 127);
        insert(94, Variable.IfStatement, 93);
        insert(94, Variable.MethodCall, 96);
        insert(94, Variable.Declare, 92);
        insert(94, Variable.Print, 95);
        insert(94, Variable.Get, 94);
        insert(94, Variable.Loop, 97);
        insert(94, Variable.Main, 128);
        insert(95, Variable.IfStatement, 93);
        insert(95, Variable.MethodCall, 96);
        insert(95, Variable.Declare, 92);
        insert(95, Variable.Print, 95);
        insert(95, Variable.Get, 94);
        insert(95, Variable.Loop, 97);
        insert(95, Variable.Main, 129);
        insert(96, Variable.IfStatement, 93);
        insert(96, Variable.MethodCall, 96);
        insert(96, Variable.Declare, 92);
        insert(96, Variable.Print, 95);
        insert(96, Variable.Get, 94);
        insert(96, Variable.Loop, 97);
        insert(96, Variable.Main, 130);
        insert(97, Variable.IfStatement, 93);
        insert(97, Variable.MethodCall, 96);
        insert(97, Variable.Declare, 92);
        insert(97, Variable.Print, 95);
        insert(97, Variable.Get, 94);
        insert(97, Variable.Loop, 97);
        insert(97, Variable.Main, 131);
        
        insert(107, Variable.code, 108);
        
        insert(109, Variable.Body, 110);
        
        insert(112, Variable.dataValLiteral, 113);
        
        insert(119, Variable.dataValLiteral, 120);
        
        insert(123, Variable.dataValLiteral, 124);
        
        insert(133, Variable.dataVal, 157);
        insert(133, Variable.Condition, 149);
        insert(133, Variable.Relational, 155);
        
        insert(140, Variable.dataValNum, 183);
        
        insert(149, Variable.Logic, 165);
        
        insert(151, Variable.IfStatement, 93);
        insert(151, Variable.MethodCall, 96);
        insert(151, Variable.Declare, 92);
        insert(151, Variable.Print, 95);
        insert(151, Variable.Get, 94);
        insert(151, Variable.Loop, 97);
        insert(151, Variable.Main, 152);
        
        insert(153, Variable.dataVal, 157);
        insert(153, Variable.Condition, 170);
        insert(153, Variable.Relational, 155);
        insert(154, Variable.dataVal, 157);
        insert(154, Variable.Condition, 172);
        insert(154, Variable.Relational, 155);
        insert(157, Variable.CondiSign, 173);
        insert(162, Variable.IfStatement, 93);
        insert(162, Variable.MethodCall, 96);
        insert(162, Variable.Declare, 92);
        insert(162, Variable.Print, 95);
        insert(162, Variable.Get, 94);
        insert(162, Variable.Loop, 97);
        insert(162, Variable.Main, 163);
        
        insert(165, Variable.dataVal, 157);
        insert(165, Variable.Condition, 198);
        insert(165, Variable.Relational, 155);
        
        insert(170, Variable.Logic, 165);
        
        insert(173, Variable.dataVal, 174);
        
        insert(175, Variable.dataVal, 176);
        insert(175, Variable.dataValNum, 197);
        insert(175, Variable.Mathematical, 178);
        
        insert(178, Variable.MathSign, 205);
        
        insert(179, Variable.Mathematical, 193);
        insert(179, Variable.dataVal, 176);
        insert(179, Variable.dataValNum, 197);
        
        insert(184, Variable.dataVal, 157);
        insert(184, Variable.Condition, 185);
        insert(184, Variable.Relational, 155);
        
        insert(185, Variable.Logic, 165);
        
        insert(186, Variable.dataValNum, 187);
        
        insert(189, Variable.IfStatement, 93);
        insert(189, Variable.MethodCall, 96);
        insert(189, Variable.Declare, 92);
        insert(189, Variable.Print, 95);
        insert(189, Variable.Get, 94);
        insert(189, Variable.Loop, 97);
        insert(189, Variable.Main, 190);
       
        insert(192, Variable.IfStatement, 93);
        insert(192, Variable.MethodCall, 96);
        insert(192, Variable.Declare, 92);
        insert(192, Variable.Print, 95);
        insert(192, Variable.Get, 94);
        insert(192, Variable.Loop, 97);
        insert(192, Variable.Main, 77);
        
        insert(193, Variable.MathSign, 205);
        
        insert(196, Variable.MathSign, 205);
        
        insert(198, Variable.Logic, 165);
        
        insert(205, Variable.dataValNum, 197);
        insert(205, Variable.Mathematical, 196);
  
    }
    
    
//    private void insert(int state, Variable lhs, String value){
//        if(row[state]==null){
//            row[state] = new HashMap<Variable, String>();
//        }
//        row[state].put(lhs, value);
//    }
    
    void insert(int state, Variable lhs, int value){
        if(row[state]==null){
            row[state] = new HashMap<Variable, Integer>();
        }
        row[state].put(lhs, value);
    }
    
    public Integer get(int state, Variable lhs){
        return row[state].get(lhs);
    }
    
}
