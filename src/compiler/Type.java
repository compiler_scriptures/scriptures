package compiler;

public enum Type {
    //these are the token types
    IF, ELSE, IS, GENESIS, RESURRECT, CREATION, EXODUS, BAPTIZE, REVELATION, PRAY, UNVEIL, 
    Char, Num, Literal, Id, Bool,
    NUMBER, STRING, BOOLEAN, CHARACTER,
    RELOPgt, RELOPlt, RELOPeq, RELOPge, RELOPle, RELOPne,
    OPplus, OPminus, OPtimes, OPneg,
    LOGOPand, LOGOPor, LOGOPnand, LOGOPnor, LOGOPnot,
    eos, WHITESPACE, 
    LEFTP, RIGHTP, LEFTSQ, RIGHTSQ, COMMA, LEFTCU, RIGHTCU,
    UNTIL, BY, FOR, THE, SUMMON, CONFIRM,
    GOOD, BAD, $, ERR,
}
