/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiler;

import java.util.HashMap;

/**
 *
 * @author Josh
 */
public class ReservedWords extends HashMap<String, Type>{
    ReservedWords(){
        this.put("IS", Type.IS);
        this.put("IF", Type.IF);
        this.put("ELSE", Type.ELSE);
        this.put("GENESIS", Type.GENESIS);
        this.put("RESURRECT", Type.RESURRECT);
        this.put("CREATION", Type.CREATION);
        this.put("EXODUS", Type.EXODUS);
        this.put("BAPTIZE", Type.BAPTIZE);
        this.put("REVELATION", Type.REVELATION);
        this.put("PRAY", Type.PRAY);
        this.put("NUMBER", Type.NUMBER);
        this.put("STRING", Type.STRING);
        this.put("BOOLEAN", Type.BOOLEAN);
        this.put("CHARACTER", Type.CHARACTER);
        this.put("UNTIL", Type.UNTIL);
        this.put("BY", Type.BY);
        this.put("FOR", Type.FOR);
        this.put("THE", Type.THE);
        this.put("CONFIRM", Type.CONFIRM);
        this.put("SUMMON", Type.SUMMON);
        this.put("GOOD", Type.GOOD);
        this.put("BAD", Type.BAD);
        this.put("UNVEIL", Type.UNVEIL);
        this.put("OR", Type.LOGOPor);
        this.put("NOR", Type.LOGOPnor);
        this.put("AND", Type.LOGOPand);
        this.put("NAND", Type.LOGOPnand);
        this.put("NOT", Type.LOGOPnot);
    }
}
