package compiler;

import java.util.HashMap;

public class Symboltable {
//    public int currentIndex;
    public HashMap<String,Identifier> list;
    public String currentScope = "Global";
    Symboltable() throws SemanticException{
        list = new HashMap();
        Identifier temp = new Identifier(new Token(Type.EXODUS, "EXODUS"));
        temp.datatype = 4;
        temp.isFinal = 0;
        temp.value = null;
        temp.list = new HashMap<>();
        insert(temp); //for main method
    }
    
//    Symboltable(Symboltable copying) throws SemanticException{ //only copy for local scopes
//        list = new HashMap();
//        for(String key : copying.list.keySet()){
//            Identifier copyingI = copying.list.get(key);
//            Identifier temp = new Identifier(new Token(Type.Id, key));
//            temp.datatype = copyingI.datatype;
//            temp.isFinal = copyingI.isFinal;
//            temp.value = copyingI.value;
//            temp.list = null;
//            insert(temp);
//        }
//    }
    
    public void insert(Identifier incoming) throws SemanticException{
//        System.out.println(currentScope);
        HashMap<String, Identifier> tempList = list;
        if(currentScope == null){
            throw new SemanticException("Undeclared procedure");
        }
        if(tempList.containsKey(incoming.name)){
            throw new SemanticException("Repeating Identifier : " + incoming.name);
            
        }
        if(currentScope.compareTo("Global") != 0){ //scope check
            if(list.get(currentScope).list != null){
                tempList = list.get(currentScope).list;
                if(tempList.containsKey(incoming.name)){
                    throw new SemanticException("Repeating Identifier : " + incoming.name);
            
                }
            }
        }
        tempList.put(incoming.name, incoming);
    }
}
