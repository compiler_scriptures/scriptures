/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiler;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Josh
 */
public class TreeNode{
    
   public int Id;
   public String SymbolName;
   public String lexeme; //only terminals would have lexeme. if not terminal then lexeme is null
   TreeNode Parent;
   ArrayList<Tree> Children;
  
   public TreeNode(int id, String data, String data2)
   {
      Id = id;
      SymbolName = data;
      lexeme = data2;
      Children = new ArrayList<>();
   }
   

}