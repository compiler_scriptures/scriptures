/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiler;

/**
 *
 * @author Josh
 */
public enum Variable {
    //these are the LHS or non-terminals of the grammar
     //lagay mo dito lahat ng variables natin. Same dapat yung casing nung nasa docu.
    SPrime, Start, FirstDiv, SecondDiv, ThirdDiv, FourthDiv, FifthDiv, mList, 
    vList, dataType, dataVal, dataValNum, dataValLiteral, dataValBool, vList2, Body,
    sBody, code, Condition, Relational, CondiSign, Logic, Mathematical, MathSign, IfStatement,
    MethodCall, Declare, Print, Get, Loop, Main, $, Init
}
