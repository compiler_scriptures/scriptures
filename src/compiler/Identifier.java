package compiler;

import java.util.HashMap;

public class Identifier {
    String name;
    String value;
    int datatype; //0 = string, 1 =  char,2 = number,3 = boolean, 4 = procedure, 5 = program name
    int isFinal; //0 = non-final, 1 = final
    public HashMap<String, Identifier> list;
    
    Identifier(Token t){
            name = t.lexeme;
    }

    Identifier() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String isDatatype(){
        switch(datatype){
            case 0 : return "string";
            case 1 : return "character";
            case 2 : return "number";
            case 3 : return "boolean";
            case 4 : return "procedure";
            case 5 : return "program name";
            default : return "err";    
        }
    }
}
